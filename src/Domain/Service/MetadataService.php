<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\DTO\Metadata\Metadata;
use App\Domain\DTO\Metadata\MetadataInterface;
use App\Domain\DTO\Metadata\Warning\Warning;

final class MetadataService
{
    private ?MetadataInterface $metadata = null;

    public function addWarning(string $message): void
    {
        if (null === $this->metadata) {
            $this->metadata = new Metadata();
        }

        $this->metadata->addWarning(new Warning($message));
    }

    public function getMetadata(): ?MetadataInterface
    {
        return $this->metadata;
    }
}
