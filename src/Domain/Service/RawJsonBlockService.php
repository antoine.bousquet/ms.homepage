<?php

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Enum\Platform;
use App\Infrastructure\Helper\TemplateEngine;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpKernel\KernelInterface;

class RawJsonBlockService
{
    public function __construct(
        private readonly TemplateEngine $templateEngine,
        private readonly KernelInterface $kernel,
        private readonly string $suggestedCategoriesFileName,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
    }

    /**
     * @throws \LogicException If the file could not be read for some reason.
     *
     * @return array<array<mixed>>
     */
    public function renderSuggestedCategoriesBlock(Platform $platform): array
    {
        $jsonFilePath = $this->getSuggestedCategoriesJsonFileAbsolutePath();
        try {
            $fileContent = $this->fileGetContents($jsonFilePath);
        } catch (\Throwable $throwable) {
            $this->logger->error(
                self::class.' error accessing content file '.$jsonFilePath,
                [
                    'class' => self::class.':'.__METHOD__,
                    'error_message' => $throwable->getMessage(),
                    'content' => $jsonFilePath,
                    'platform_id' => $platform->value,
                ]
            );

            throw $throwable;
        }

        $fileContent = $this->templateEngine->render($fileContent, ['{{ environment_string }}' => $this->getEnvironmentString()]);
        /**
         * @var array<string, array<mixed>> $denormalizedContent
         */
        $denormalizedContent = json_decode($fileContent, true);

        if (\JSON_ERROR_NONE !== json_last_error()) {
            $message = sprintf('Error decoding content from JSON file "%s"', $jsonFilePath);
            $this->logger->error(
                $message,
                [
                    'class' => self::class.':'.__METHOD__,
                    'error_message' => $errorMessage = 'JSON file '.$jsonFilePath.' could not be decoded',
                    'content' => $jsonFilePath,
                    'platform_id' => $platform->value,
                ]
            );

            throw new \LogicException($message);
        }

        if (false === \array_key_exists(mb_strtolower($platform->value), $denormalizedContent)) {
            $message = sprintf('No content available for platform "%s" in file "%s"', $platform->value, $jsonFilePath);
            $this->logger->error(
                $message,
                [
                    'class' => self::class.':'.__METHOD__,
                    'content' => $jsonFilePath,
                    'platform_id' => $platform->value,
                ]
            );

            return [];
        }

        /**
         * @var array<array<mixed>>
         */
        $denormalizedPlatformContent = $denormalizedContent[mb_strtolower($platform->value)];

        return $denormalizedPlatformContent;
    }

    protected function fileGetContents(string $filePath): string
    {
        $fileContent = @file_get_contents($filePath);
        if (false === $fileContent) {
            $error = error_get_last();

            throw new \LogicException((null !== $error) ? $error['message'] : 'Unavailable error message');
        }

        return $fileContent;
    }

    private function getSuggestedCategoriesJsonFileAbsolutePath(): string
    {
        return sprintf(
            '%s/data/%s.json',
            $this->kernel->getProjectDir(),
            $this->suggestedCategoriesFileName,
        );
    }

    private function getEnvironmentString(): string
    {
        $environment = mb_strtolower($this->kernel->getEnvironment());

        $this->logger->info(sprintf('Original environment string is "%s"', $environment));

        switch ($environment) {
            case 'test':
                return '.test';
            case 'dev':
            case 'int':
                return '.int';
            case 'stg':
                return '.stg';
            case 'prd':
            case 'prod':
                return '';
        }

        $this->logger->error(sprintf('Unrecognized environment "%s", fallback to production environment string.', $environment));

        return '';
    }
}
