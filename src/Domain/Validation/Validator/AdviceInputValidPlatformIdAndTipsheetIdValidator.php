<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator;

use App\Domain\DTO\AdviceInputDTO;
use App\Domain\Validation\Constraint\AdviceInputValidPlatformIdAndTipsheetId;
use App\Infrastructure\Repository\TipsheetRepositoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

final class AdviceInputValidPlatformIdAndTipsheetIdValidator extends ConstraintValidator
{
    public function __construct(private readonly TipsheetRepositoryInterface $tipsheetRepository)
    {
    }

    /**
     * @param AdviceInputDTO                          $adviceInput
     * @param AdviceInputValidPlatformIdAndTipsheetId $constraint
     */
    public function validate(mixed $adviceInput, Constraint $constraint): void
    {
        Assert::isInstanceOf($adviceInput, AdviceInputDTO::class);
        Assert::isInstanceOf($constraint, AdviceInputValidPlatformIdAndTipsheetId::class);

        $tipsheet = $this->tipsheetRepository->findOneById($adviceInput->getTipsheetId());

        if (null === $tipsheet || $adviceInput->getTipsheetId() !== $tipsheet->getId()) {
            $this->context->addViolation('No tipsheet found for ID #{{ tipsheet_id }}', [
                '{{ tipsheet_id }}' => $adviceInput->getTipsheetId(),
            ]);
        } elseif (mb_strtoupper($adviceInput->getPlatformId()) !== $tipsheet->getPlatform()) {
            $this->context->addViolation(
                'Tipsheet with ID #{{ tipsheet_id }} is associated with platform "{{ input_platform_id }}", given "{{ platform_id }}" instead',
                [
                    '{{ tipsheet_id }}' => $adviceInput->getTipsheetId(),
                    '{{ platform_id }}' => $adviceInput->getPlatformId(),
                    '{{ input_platform_id }}' => $tipsheet->getPlatform(),
                ]
            );
        } elseif (false === $tipsheet->isVisible()) {
            $this->context->addViolation('Tipsheet with ID #{{ tipsheet_id }} is not visible', [
                '{{ tipsheet_id }}' => $adviceInput->getTipsheetId(),
            ]);
        }
    }
}
