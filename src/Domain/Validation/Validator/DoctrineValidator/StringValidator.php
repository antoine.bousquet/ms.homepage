<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Type;

final class StringValidator implements DoctrineValidatorInterface
{
    /**
     * @param array{length?: int} $propertyMapping
     *
     * @return array<Constraint>
     */
    public function __invoke(array $propertyMapping): array
    {
        $sequence = [];
        $sequence[] = new Type('string');

        if (isset($propertyMapping['length'])) {
            if (!\is_int($propertyMapping['length'])) {
                throw new \LogicException(sprintf('The "length" provided with the following annotation must be an integer: %s', json_encode($propertyMapping)));
            }

            $sequence[] = new Length(null, null, $propertyMapping['length']);
        }

        return [new Sequentially([
            'constraints' => $sequence,
        ])];
    }

    /**
     * {@inheritDoc}
     */
    public function handle(array $propertyMapping): bool
    {
        return 'string' === $propertyMapping['type'];
    }

    public function getPriority(): int
    {
        return 1;
    }
}
