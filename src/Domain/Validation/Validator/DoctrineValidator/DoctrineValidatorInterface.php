<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

use Symfony\Component\Validator\Constraint;

interface DoctrineValidatorInterface
{
    /**
     * @param array<string, mixed> $propertyMapping
     *
     * @return array<Constraint>
     */
    public function __invoke(array $propertyMapping): array;

    /**
     * @param array<string, mixed> $propertyMapping
     */
    public function handle(array $propertyMapping): bool;

    public function getPriority(): int;
}
