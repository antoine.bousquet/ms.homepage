<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\Type;

abstract class AbstractNumberValidator implements DoctrineValidatorInterface
{
    public const MINIMUM_SIGNED = null;
    public const MAXIMUM_SIGNED = null;
    public const MAXIMUM_UNSIGNED = null;

    /**
     * @param array{options?: array{unsigned?: bool}} $propertyMapping
     *
     * @return array<Constraint>
     */
    public function __invoke(array $propertyMapping): array
    {
        $sequence = [];
        $sequence[] = new Type('int');

        if (true === ($propertyMapping['options']['unsigned'] ?? false)) {
            $sequence[] = new Range([
                'min' => 0,
                'max' => static::MAXIMUM_UNSIGNED,
            ]);
        } else {
            $sequence[] = new Range([
                'min' => static::MINIMUM_SIGNED,
                'max' => static::MAXIMUM_SIGNED,
            ]);
        }

        return [new Sequentially([
            'constraints' => $sequence,
        ])];
    }

    public function getPriority(): int
    {
        return 1;
    }
}
