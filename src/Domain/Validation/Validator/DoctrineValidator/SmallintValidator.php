<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

final class SmallintValidator extends AbstractNumberValidator
{
    public const MINIMUM_SIGNED = -32768;
    public const MAXIMUM_SIGNED = 32767;
    public const MAXIMUM_UNSIGNED = 65535;

    public function handle(array $propertyMapping): bool
    {
        return 'smallint' === $propertyMapping['type'];
    }
}
