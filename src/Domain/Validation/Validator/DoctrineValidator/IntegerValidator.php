<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

final class IntegerValidator extends AbstractNumberValidator
{
    public const MINIMUM_SIGNED = -2147483648;
    public const MAXIMUM_SIGNED = 2147483647;
    public const MAXIMUM_UNSIGNED = 4294967295;

    public function handle(array $propertyMapping): bool
    {
        return 'integer' === $propertyMapping['type'];
    }
}
