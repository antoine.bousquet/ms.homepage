<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator\DoctrineValidator;

use Symfony\Component\Validator\Constraints\NotNull;

final class NotNullValidator implements DoctrineValidatorInterface
{
    public function __invoke(array $propertyMapping): array
    {
        return [
            new NotNull(),
        ];
    }

    public function handle(array $propertyMapping): bool
    {
        return false === ($propertyMapping['nullable'] ?? false);
    }

    public function getPriority(): int
    {
        return 2;
    }
}
