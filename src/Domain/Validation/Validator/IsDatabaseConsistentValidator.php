<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator;

use App\Domain\Validation\Constraint\IsDatabaseConsistent;
use App\Domain\Validation\Constraint\UniqueEntity;
use App\Domain\Validation\Validator\DoctrineValidator\DoctrineValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

final class IsDatabaseConsistentValidator extends ConstraintValidator
{
    /**
     * @var array<string>
     */
    private array $invalidProperties;

    /**
     * @param iterable<DoctrineValidatorInterface> $doctrineValidators
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PropertyAccessorInterface $propertyAccessor,
        private readonly iterable $doctrineValidators
    ) {
    }

    /**
     * @param object               $object
     * @param IsDatabaseConsistent $isDatabaseConsistent
     */
    public function validate(mixed $object, Constraint $isDatabaseConsistent): void
    {
        Assert::isInstanceOf($isDatabaseConsistent, IsDatabaseConsistent::class);
        Assert::object($object, 'This constraint can only be applied to objects.');

        $targetEntityClass = $isDatabaseConsistent->getTargetEntityClass() ?? \get_class($object);
        $metadata = $this->entityManager->getClassMetadata($targetEntityClass);

        $this->invalidProperties = [];
        $this->validateProperties($object, $metadata);
        $this->validateUniqueIndices($object, $metadata);
    }

    private function validateProperties(object $object, ClassMetadata $metadata): void
    {
        foreach ($metadata->getFieldNames() as $fieldName) {
            $fieldMapping = $metadata->getFieldMapping($fieldName);
            try {
                /**
                 * @var float|int|string $fieldValue
                 */
                $fieldValue = $this->propertyAccessor->getValue($object, $fieldName);
            } catch (AccessException $exception) {
                continue;
            }

            $this->validateProperty($fieldName, $fieldValue, $fieldMapping);
        }
    }

    /**
     * @param array<string, mixed> $fieldMapping
     */
    private function validateProperty(string $fieldName, mixed $fieldValue, array $fieldMapping): void
    {
        if (false !== ($fieldMapping['unique'] ?? false)) {
            throw new \LogicException(sprintf('The field "%s" is marked unique and this needs to be checked, but unicity checks have not yet been implemented in "%s".', $fieldName, self::class));
        }

        $validators = $this->getDoctrineValidators($fieldMapping);

        $constraints = [];
        foreach ($validators as $validator) {
            $constraints = [...$constraints, ...$validator($fieldMapping)];
        }
        if ([] === $constraints) {
            return;
        }

        $fieldViolations = $this->context->getValidator()->validate($fieldValue, $constraints);
        if (0 < \count($fieldViolations)) {
            $this->invalidProperties[] = $fieldName;
        }
        foreach ($fieldViolations as $fieldViolation) {
            $this->context
                ->buildViolation($fieldViolation->getMessageTemplate(), $fieldViolation->getParameters())
                ->atPath($fieldName)
                ->addViolation();
        }
    }

    private function validateUniqueIndices(object $object, ClassMetadata $metadata): void
    {
        $uniqueEntities = [];
        foreach ($metadata->table['uniqueConstraints'] ?? [] as $uniqueConstraint) {
            $fieldNames = [];
            foreach ($uniqueConstraint['columns'] as $columnName) {
                $fieldName = $metadata->getFieldForColumn($columnName);
                // If one of the columns is invalid, don't check for unicity
                if (\in_array($fieldName, $this->invalidProperties, true)) {
                    continue 2;
                }
                try {
                    $this->propertyAccessor->getValue($object, $fieldName);
                } catch (AccessException $exception) {
                    // If one of the columns is not found in the object, skip this unicity constraint
                    continue 2;
                }
                $fieldNames[] = $fieldName;
            }

            $uniqueEntities[] = new UniqueEntity([
                'fields' => $fieldNames,
                'targetEntityClass' => $metadata->getName(),
            ]);
        }
        if ([] === $uniqueEntities) {
            return;
        }

        $violations = $this->context->getValidator()->validate($object, $uniqueEntities);
        foreach ($violations as $violation) {
            $this->context->addViolation($violation->getMessageTemplate(), $violation->getParameters());
        }
    }

    /**
     * @param array<string, mixed> $fieldMapping
     *
     * @return array<DoctrineValidatorInterface>
     */
    private function getDoctrineValidators(array $fieldMapping): array
    {
        $validators = [];
        foreach ($this->doctrineValidators as $doctrineValidator) {
            if ($doctrineValidator->handle($fieldMapping)) {
                $validators[] = $doctrineValidator;
            }
        }

        usort($validators, function (DoctrineValidatorInterface $validatorA, DoctrineValidatorInterface $validatorB): int {
            return $validatorB->getPriority() - $validatorA->getPriority();
        });

        return $validators;
    }
}
