<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator;

use App\Domain\Validation\Constraint\ValidBrandId;
use App\Infrastructure\Repository\BrandDataRepositoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

final class ValidBrandIdValidator extends ConstraintValidator
{
    public function __construct(private readonly BrandDataRepositoryInterface $repository)
    {
    }

    /**
     * @param positive-int $brandId
     * @param ValidBrandId $validBrandId
     */
    public function validate(mixed $brandId, Constraint $validBrandId): void
    {
        Assert::isInstanceOf($validBrandId, ValidBrandId::class);
        Assert::positiveInteger($brandId);

        $brand = $this->repository->findOneById((string) $brandId);
        if (null === $brand) {
            $this->context->addViolation(sprintf('No brand found with ID #%d', $brandId));
        }
    }
}
