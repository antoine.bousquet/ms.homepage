<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator;

use App\Domain\DTO\PromotedProductInputDTO;
use App\Domain\Validation\Constraint\PromotedProductInputValidPlatformIdAndModelId;
use App\Infrastructure\Repository\ProductRepositoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

final class PromotedProductInputValidPlatformIdAndModelIdValidator extends ConstraintValidator
{
    public function __construct(private readonly ProductRepositoryInterface $productRepository)
    {
    }

    /**
     * @param PromotedProductInputDTO                       $promotedProductInput
     * @param PromotedProductInputValidPlatformIdAndModelId $constraint
     */
    public function validate(mixed $promotedProductInput, Constraint $constraint): void
    {
        Assert::isInstanceOf($promotedProductInput, PromotedProductInputDTO::class);
        Assert::isInstanceOf($constraint, PromotedProductInputValidPlatformIdAndModelId::class);

        $product = $this->productRepository->findOneByModelIdAndPlatform($promotedProductInput->getModelId(), $promotedProductInput->getPlatformId());

        $modelIdExists = (null !== $product && $promotedProductInput->getModelId() === $product->getModelId());
        if (!$modelIdExists) {
            $this->context->addViolation('No model found for ID #{{ model_id }} and platform "{{ platform_id }}"', [
                '{{ model_id }}' => $promotedProductInput->getModelId(),
                '{{ platform_id }}' => $promotedProductInput->getPlatformId(),
            ]);
        }
    }
}
