<?php

declare(strict_types=1);

namespace App\Domain\Validation\Validator;

use App\Domain\Validation\Constraint\UniqueEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

final class UniqueEntityValidator extends ConstraintValidator
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly PropertyAccessorInterface $propertyAccessor,
    ) {
    }

    /**
     * @param object       $object
     * @param UniqueEntity $uniqueEntity
     */
    public function validate(mixed $object, Constraint $uniqueEntity): void
    {
        Assert::isInstanceOf($uniqueEntity, UniqueEntity::class);
        Assert::object($object, 'This constraint can only be applied to objects.');

        $repository = $this->entityManager->getRepository($uniqueEntity->getTargetEntityClass() ?? \get_class($object));

        $criteria = [];
        foreach ($uniqueEntity->getFields() as $field) {
            $criteria[$field] = $this->propertyAccessor->getValue($object, $field);
        }

        $entity = $repository->findOneBy($criteria);
        if (null !== $entity) {
            $this->context->addViolation($uniqueEntity->getMessage(), [
                '{{ set }}' => implode(', ', $uniqueEntity->getFields()),
            ]);
        }
    }
}
