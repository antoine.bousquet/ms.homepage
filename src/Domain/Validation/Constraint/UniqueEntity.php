<?php

declare(strict_types=1);

namespace App\Domain\Validation\Constraint;

use App\Domain\Entity\EntityInterface;
use App\Domain\Validation\Validator\UniqueEntityValidator;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueEntity extends Constraint
{
    protected string $message = 'The provided data set on {{ set }} is already used.';

    /**
     * @var array<string>
     */
    protected array $fields;

    /**
     * @var class-string<EntityInterface>|null
     */
    protected ?string $targetEntityClass = null;

    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array<string>
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return class-string|null
     */
    public function getTargetEntityClass(): ?string
    {
        return $this->targetEntityClass;
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy(): string
    {
        return UniqueEntityValidator::class;
    }
}
