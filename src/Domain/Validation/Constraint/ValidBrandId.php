<?php

declare(strict_types=1);

namespace App\Domain\Validation\Constraint;

use App\Domain\Validation\Validator\ValidBrandIdValidator;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class ValidBrandId extends Constraint
{
    public function validatedBy(): string
    {
        return ValidBrandIdValidator::class;
    }
}
