<?php

declare(strict_types=1);

namespace App\Domain\Validation\Constraint;

use App\Domain\Validation\Validator\AdviceInputValidPlatformIdAndTipsheetIdValidator;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
final class AdviceInputValidPlatformIdAndTipsheetId extends Constraint
{
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy(): string
    {
        return AdviceInputValidPlatformIdAndTipsheetIdValidator::class;
    }
}
