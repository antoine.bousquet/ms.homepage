<?php

declare(strict_types=1);

namespace App\Domain\Validation\Constraint;

use App\Domain\Validation\Validator\IsDatabaseConsistentValidator;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_CLASS)]
final class IsDatabaseConsistent extends Constraint
{
    /**
     * @var class-string|null
     */
    protected ?string $targetEntityClass = null;

    public function getDefaultOption(): string
    {
        return 'targetEntityClass';
    }

    /**
     * @return class-string|null
     */
    public function getTargetEntityClass(): ?string
    {
        return $this->targetEntityClass;
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy(): string
    {
        return IsDatabaseConsistentValidator::class;
    }
}
