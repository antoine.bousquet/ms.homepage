<?php

declare(strict_types=1);

namespace App\Domain\Converter;

use App\Domain\DTO\DTOInterface;
use App\Domain\Entity\EntityInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

final class DtoToEntityConverter
{
    private const CACHE_ITEM_PREFIX = 'dto_properties.';

    public function __construct(
        private readonly PropertyAccessorInterface $accessor,
        private readonly AdapterInterface $cache = new NullAdapter(),
    ) {
    }

    public function convert(EntityInterface $entity, DTOInterface $dto): EntityInterface
    {
        $properties = $this->getDtoProperties(\get_class($dto));
        foreach ($properties as $property) {
            $value = $this->accessor->getValue($dto, $property);
            $this->accessor->setValue($entity, $property, $value);
        }

        return $entity;
    }

    /**
     * @param class-string<DTOInterface> $dtoClassName
     *
     * @return array<string> Camel-cased property names
     */
    private function getDtoProperties(string $dtoClassName): array
    {
        $cacheClassName = str_replace('\\', '_', $dtoClassName);
        $cacheKey = self::CACHE_ITEM_PREFIX.$cacheClassName;
        $item = $this->cache->getItem($cacheKey);

        if (!$item->isHit()) {
            $class = new \ReflectionClass($dtoClassName);
            $properties = $class->getProperties();
            $properties = array_map(static function (\ReflectionProperty $property): string {
                return $property->getName();
            }, $properties);

            $item->set($properties);
        }

        /**
         * @var array<string> $return
         */
        $return = $item->get();

        return $return;
    }
}
