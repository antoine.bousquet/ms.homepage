<?php

declare(strict_types=1);

namespace App\Domain\Entity\Homepage;

use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\Traits\IdTrait;
use App\Domain\Entity\Traits\PlatformIdTrait;
use App\Infrastructure\Repository\PromotedProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PromotedProductRepository::class)]
#[ORM\Table(name: 'promoted_product')]
#[ORM\Index(columns: ['model_id'], name: 'model_id_idx')]
class PromotedProduct implements AdministrableResourceEntityInterface
{
    use IdTrait;
    use PlatformIdTrait;

    /**
     * @var positive-int
     */
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private int $modelId;

    /**
     * @var positive-int
     */
    #[ORM\Column(type: 'smallint', options: ['unsigned' => true])]
    private int $position;

    /**
     * @param positive-int $modelId
     */
    public function setModelId(int $modelId): void
    {
        $this->modelId = $modelId;
    }

    /**
     * @return positive-int
     */
    public function getModelId(): int
    {
        return $this->modelId;
    }

    /**
     * @param positive-int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return positive-int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}
