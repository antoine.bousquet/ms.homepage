<?php

declare(strict_types=1);

namespace App\Domain\Entity\Homepage;

use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\Traits\IdTrait;
use App\Infrastructure\Repository\AdministrationLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

#[ORM\Entity(repositoryClass: AdministrationLogRepository::class)]
#[ORM\Table(name: 'administration_log')]
class AdministrationLog implements EntityInterface
{
    use IdTrait;

    final public const ACTION_CREATE = 'C';
    final public const ACTION_UPDATE = 'U';
    final public const ACTION_DELETE = 'D';
    final public const ACTIONS = [
        self::ACTION_CREATE,
        self::ACTION_UPDATE,
        self::ACTION_DELETE,
    ];

    #[ORM\Column(type: 'string', length: 255)]
    private string $username;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeInterface $dateTime;

    #[ORM\Column(type: 'string', length: 255)]
    private string $resourceName;

    #[ORM\Column(type: 'guid')]
    private string $resourceId;

    #[ORM\Column(type: 'string', length: 1)]
    private string $action;

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setDateTime(\DateTimeInterface $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    public function getDateTime(): \DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setResourceName(string $resourceName): void
    {
        $this->resourceName = $resourceName;
    }

    public function getResourceName(): string
    {
        return $this->resourceName;
    }

    public function setResourceId(string $resourceId): void
    {
        $this->resourceId = $resourceId;
    }

    public function getResourceId(): string
    {
        return $this->resourceId;
    }

    public function setAction(string $action): void
    {
        Assert::inArray($action, self::ACTIONS);

        $this->action = $action;
    }

    public function getAction(): string
    {
        return $this->action;
    }
}
