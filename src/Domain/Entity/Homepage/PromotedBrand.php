<?php

declare(strict_types=1);

namespace App\Domain\Entity\Homepage;

use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\Traits\IdTrait;
use App\Domain\Entity\Traits\PlatformIdTrait;
use App\Infrastructure\Repository\PromotedBrandRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PromotedBrandRepository::class)]
#[ORM\Table(name: 'promoted_brand')]
#[ORM\Index(columns: ['brand_id'], name: 'brand_id_idx')]
class PromotedBrand implements AdministrableResourceEntityInterface
{
    use IdTrait;
    use PlatformIdTrait;

    /**
     * @var positive-int
     */
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private int $brandId;

    #[ORM\Column(type: 'smallint', options: ['unsigned' => true])]
    private int $position;

    /**
     * @param positive-int $brandId
     */
    public function setBrandId(int $brandId): void
    {
        $this->brandId = $brandId;
    }

    /**
     * @return positive-int
     */
    public function getBrandId(): int
    {
        return $this->brandId;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getPosition(): int
    {
        return $this->position;
    }
}
