<?php

declare(strict_types=1);

namespace App\Domain\Entity\Homepage;

use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\Traits\IdTrait;
use App\Domain\Entity\Traits\PlatformIdTrait;
use App\Infrastructure\Repository\AdviceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdviceRepository::class)]
#[ORM\Table(name: 'advice')]
#[ORM\Index(columns: ['tipsheet_id'], name: 'tipsheet_id_idx')]
class Advice implements AdministrableResourceEntityInterface
{
    use IdTrait;
    use PlatformIdTrait;

    /**
     * @var positive-int
     */
    #[ORM\Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    private int $tipsheetId;

    #[ORM\Column(type: 'smallint', nullable: false, options: ['unsigned' => true])]
    private int $position;

    /**
     * @param positive-int $tipsheetId
     */
    public function setTipsheetId(int $tipsheetId): void
    {
        $this->tipsheetId = $tipsheetId;
    }

    /**
     * @return positive-int
     */
    public function getTipsheetId(): int
    {
        return $this->tipsheetId;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getPosition(): int
    {
        return $this->position;
    }
}
