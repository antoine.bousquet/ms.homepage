<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use Webmozart\Assert\Assert;

class ListResult
{
    /**
     * @param iterable<EntityInterface> $items
     */
    public function __construct(
        private readonly iterable $items,
        private readonly int $count,
    ) {
        Assert::allIsInstanceOf($items, EntityInterface::class);
        Assert::greaterThanEq($count, 0);
    }

    /**
     * @return iterable<EntityInterface>
     */
    public function getItems(): iterable
    {
        return $this->items;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
