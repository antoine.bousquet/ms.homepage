<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class Product implements EntityInterface
{
    public function __construct(
        private readonly int $modelId,
        private readonly string $platform,
        private readonly ?int $discountPercentage,
    ) {
    }

    public function getModelId(): int
    {
        return $this->modelId;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function getDiscountPercentage(): ?int
    {
        return $this->discountPercentage;
    }
}
