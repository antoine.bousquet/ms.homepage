<?php

declare(strict_types=1);

namespace App\Domain\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait PlatformIdTrait
{
    #[ORM\Column(type: 'string', length: 2)]
    private string $platformId;

    public function setPlatformId(string $platformId): void
    {
        $this->platformId = $platformId;
    }

    public function getPlatformId(): string
    {
        return $this->platformId;
    }
}
