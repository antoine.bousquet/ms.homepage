<?php

declare(strict_types=1);

namespace App\Domain\Entity;

interface AdministrableResourceEntityInterface extends EntityInterface
{
    public function getId(): string;
}
