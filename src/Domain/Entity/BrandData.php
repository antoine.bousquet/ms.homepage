<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class BrandData implements EntityInterface
{
    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly string $imageUrl,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }
}
