<?php

declare(strict_types=1);

namespace App\Domain\Entity;

class Tipsheet implements EntityInterface
{
    public function __construct(
        private readonly int $id,
        private readonly string $platform,
        private readonly bool $visible,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }
}
