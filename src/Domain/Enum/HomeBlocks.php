<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum HomeBlocks: string
{
    case Advice = 'advice';
    case Brands = 'brands';
    case PromotedProducts = 'promoted_products';
    case SuggestedCategories = 'suggested_categories';

    /**
     * @return array<string>
     */
    public static function casesString(): array
    {
        return array_map(static function (HomeBlocks $case): string {
            return $case->value;
        }, self::cases());
    }
}
