<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum Market: string
{
    case B2B = 'B2B';
    case B2C = 'B2C';

    /**
     * @return array<string>
     */
    public static function casesString(): array
    {
        return array_map(static function (Market $case): string {
            return $case->value;
        }, self::cases());
    }
}
