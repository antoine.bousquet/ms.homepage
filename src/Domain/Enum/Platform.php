<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum Platform: string
{
    case FRANCE = 'FR';
    case GREAT_BRITAIN = 'GB';
    case ITALY = 'IT';
    case SPAIN = 'ES';
    case DEUTSCHLAND = 'DE';

    /**
     * @return array<string>
     */
    public static function casesString(): array
    {
        return array_map(static function (Platform $case): string {
            return $case->value;
        }, self::cases());
    }
}
