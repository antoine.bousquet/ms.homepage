<?php

declare(strict_types=1);

namespace App\Domain\Enum;

enum Channel: string
{
    case WEB = 'WEB';
    case MOBILE = 'MOBILE';

    /**
     * @return array<string>
     */
    public static function casesString(): array
    {
        return array_map(static function (Channel $case): string {
            return $case->value;
        }, self::cases());
    }
}
