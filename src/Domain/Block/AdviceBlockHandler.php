<?php

declare(strict_types=1);

namespace App\Domain\Block;

use App\Domain\DTO\AdviceOutputDTO;
use App\Domain\DTO\AdviceOutputFactory;
use App\Domain\Entity\Homepage\Advice;
use App\Infrastructure\Repository\AdviceRepositoryInterface;

final class AdviceBlockHandler extends AbstractBlockHandler
{
    protected const BLOCK_NAME = 'advice';

    public function __construct(
        private readonly AdviceOutputFactory $factory,
        private readonly AdviceRepositoryInterface $repository,
    ) {
    }

    public function getContent(string $platformCode): AdviceOutputDTO
    {
        /**
         * @var array<Advice> $adviceList
         */
        $adviceList = $this->repository->findBy(
            ['platformId' => mb_strtoupper($platformCode)],
            ['position' => 'ASC'],
        );

        return $this->factory->createFromAdviceList($adviceList);
    }
}
