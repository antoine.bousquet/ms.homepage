<?php

declare(strict_types=1);

namespace App\Domain\Block;

use App\Domain\DTO\BrandOutputDTO;
use App\Domain\DTO\BrandOutputFactory;
use App\Domain\Entity\Homepage\PromotedBrand;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;

final class BrandsBlockHandler extends AbstractBlockHandler
{
    protected const BLOCK_NAME = 'brands';

    public function __construct(
        private readonly BrandOutputFactory $factory,
        private readonly PromotedBrandRepositoryInterface $repository,
    ) {
    }

    public function getContent(string $platformCode): BrandOutputDTO
    {
        /**
         * @var array<PromotedBrand> $promotedBrands
         */
        $promotedBrands = $this->repository->findBy(
            ['platformId' => mb_strtoupper($platformCode)],
            ['position' => 'ASC'],
        );

        return $this->factory->createFromPromotedBrandList($promotedBrands);
    }
}
