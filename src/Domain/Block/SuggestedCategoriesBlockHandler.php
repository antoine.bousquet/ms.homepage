<?php

declare(strict_types=1);

namespace App\Domain\Block;

use App\Domain\Enum\Platform;
use App\Domain\Service\RawJsonBlockService;

final class SuggestedCategoriesBlockHandler extends AbstractBlockHandler
{
    protected const BLOCK_NAME = 'suggested_categories';

    public function __construct(private readonly RawJsonBlockService $rawJsonBlockService)
    {
    }

    /**
     * @return array<array<mixed>>
     */
    public function getContent(string $platformCode): array
    {
        $platformCode = mb_strtoupper($platformCode);

        return $this->rawJsonBlockService->renderSuggestedCategoriesBlock(Platform::from($platformCode));
    }
}
