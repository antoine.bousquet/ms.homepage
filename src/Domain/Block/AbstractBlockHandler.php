<?php

declare(strict_types=1);

namespace App\Domain\Block;

abstract class AbstractBlockHandler implements BlockHandlerInterface
{
    protected const BLOCK_NAME = null;

    public function getName(): string
    {
        if (null === static::BLOCK_NAME) {
            throw new \LogicException(sprintf('Enabled to call getName() in class %s, constant BLOCK_NAME is missing', __CLASS__));
        }

        return static::BLOCK_NAME;
    }
}
