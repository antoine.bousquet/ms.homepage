<?php

declare(strict_types=1);

namespace App\Domain\Block;

interface BlockHandlerInterface
{
    public function getContent(string $platformCode): mixed;

    public function getName(): string;
}
