<?php

declare(strict_types=1);

namespace App\Domain\Block;

use App\Domain\DTO\PromotedProductOutputDTO;
use App\Domain\DTO\PromotedProductOutputFactory;
use App\Domain\Entity\Homepage\PromotedProduct;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;

final class PromotedProductsBlockHandler extends AbstractBlockHandler
{
    protected const BLOCK_NAME = 'promoted_products';

    public function __construct(
        private readonly PromotedProductOutputFactory $factory,
        private readonly PromotedProductRepositoryInterface $repository,
    ) {
    }

    public function getContent(string $platformCode): PromotedProductOutputDTO
    {
        /**
         * @var array<PromotedProduct> $promotedProducts
         */
        $promotedProducts = $this->repository->findBy(
            ['platformId' => mb_strtoupper($platformCode)],
            ['position' => 'ASC'],
        );

        return $this->factory->createFromPromotedProductList($promotedProducts);
    }
}
