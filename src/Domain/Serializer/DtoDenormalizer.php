<?php

declare(strict_types=1);

namespace App\Domain\Serializer;

use App\Domain\DTO\DTOInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class DtoDenormalizer implements DenormalizerInterface
{
    private const CACHE_KEY_PREFIX = 'serializer.dto_denormalizer.';

    public function __construct(
        private readonly NameConverterInterface $nameConverter,
        private readonly AdapterInterface $cache = new NullAdapter(),
    ) {
    }

    /**
     * @param class-string<DTOInterface> $type
     * @param array<string, mixed>       $data
     * @param array<string, mixed>       $context
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): DTOInterface
    {
        $argumentsOrder = $this->getConstructorArgumentsOrder($type);
        $arguments = [];
        foreach ($argumentsOrder as $argumentName) {
            $serializedName = $this->nameConverter->normalize($argumentName);
            $arguments[] = $data[$serializedName] ?? null;
        }

        return new $type(...$arguments);
    }

    /**
     * @param array<string, mixed> $data
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        if (!class_exists($type) || false === ($interfaces = class_implements($type))) {
            return false;
        }

        return \in_array(DTOInterface::class, $interfaces, true);
    }

    /**
     * @return array<string>
     */
    private function getConstructorArgumentsOrder(string $type): array
    {
        $cacheKey = self::CACHE_KEY_PREFIX.str_replace('\\', '_', $type);
        $cacheItem = $this->cache->getItem($cacheKey);

        if (!$cacheItem->isHit()) {
            $method = new \ReflectionMethod($type, '__construct');
            $parameters = $method->getParameters();
            $parameters = array_map(function (\ReflectionParameter $parameter): string {
                return $parameter->getName();
            }, $parameters);

            $cacheItem->set($parameters);
        }

        /**
         * @var array<string> $order
         */
        $order = $cacheItem->get();

        return $order;
    }
}
