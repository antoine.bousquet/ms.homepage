<?php

declare(strict_types=1);

namespace App\Domain\DTO\Metadata;

use App\Domain\DTO\Metadata\Warning\Warning;

final class Metadata implements MetadataInterface
{
    /**
     * @var array<Warning>|null $warnings
     */
    private ?array $warnings;

    /**
     * @return array<Warning>|null
     */
    public function getWarnings(): ?array
    {
        return $this->warnings;
    }

    public function addWarning(Warning $warning): void
    {
        $this->warnings[] = $warning;
    }
}
