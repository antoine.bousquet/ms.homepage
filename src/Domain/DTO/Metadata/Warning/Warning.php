<?php

declare(strict_types=1);

namespace App\Domain\DTO\Metadata\Warning;

final class Warning
{
    public function __construct(private readonly string $message)
    {
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
