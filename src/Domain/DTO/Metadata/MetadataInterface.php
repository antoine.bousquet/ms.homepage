<?php

declare(strict_types=1);

namespace App\Domain\DTO\Metadata;

use App\Domain\DTO\Metadata\Warning\Warning;

interface MetadataInterface
{
    /**
     * @return array<Warning>|null
     */
    public function getWarnings(): ?array;

    public function addWarning(Warning $warning): void;
}
