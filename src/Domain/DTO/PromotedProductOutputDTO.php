<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

final class PromotedProductOutputDTO implements DTOInterface
{
    /**
     * @param array<PromotedProductOutputItemDTO> $items
     */
    public function __construct(
        #[Groups(['GET_CONFIGURATION'])]
        private readonly array $items,
    ) {
    }

    /**
     * @return array<PromotedProductOutputItemDTO>
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
