<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

final class PromotedProductOutputItemDTO implements DTOInterface
{
    /**
     * @param positive-int $modelId
     * @param positive-int $position
     */
    public function __construct(
        #[Groups(['GET_CONFIGURATION'])]
        private readonly int $modelId,
        #[Groups(['GET_CONFIGURATION'])]
        private readonly int $position,
    ) {
    }

    public function getModelId(): int
    {
        return $this->modelId;
    }

    public function getPosition(): int
    {
        return $this->position;
    }
}
