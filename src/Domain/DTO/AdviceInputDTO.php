<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\Advice;
use App\Domain\Enum\Platform;
use App\Domain\Validation\Constraint as ManoManoAssert;
use Symfony\Component\Validator\Constraints as Assert;

#[Assert\GroupSequence(['AdviceInputDTO', 'Business', 'CrossFields'])]
#[ManoManoAssert\IsDatabaseConsistent(Advice::class)]
#[ManoManoAssert\AdviceInputValidPlatformIdAndTipsheetId(groups: ['CrossFields'])]
final class AdviceInputDTO implements DTOInterface
{
    /**
     * @param string $platformId
     * @param int    $tipsheetId
     * @param int    $position
     */
    public function __construct(
        #[Assert\Choice(callback: [Platform::class, 'casesString'], groups: ['Business'])]
        private readonly mixed $platformId,
        private readonly mixed $tipsheetId,
        #[Assert\GreaterThan(value: 0, groups: ['Business'])]
        private readonly mixed $position,
    ) {
    }

    /**
     * @return string
     */
    public function getPlatformId(): mixed
    {
        return $this->platformId;
    }

    /**
     * @return int
     */
    public function getTipsheetId(): mixed
    {
        return $this->tipsheetId;
    }

    /**
     * @return int
     */
    public function getPosition(): mixed
    {
        return $this->position;
    }
}
