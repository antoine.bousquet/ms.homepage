<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\PromotedProduct;
use App\Domain\Enum\Platform;
use App\Domain\Validation\Constraint as ManoManoAssert;
use Symfony\Component\Validator\Constraints as Assert;

#[Assert\GroupSequence(['PromotedProductInputDTO', 'Business', 'CrossFields'])]
#[ManoManoAssert\IsDatabaseConsistent(PromotedProduct::class)]
#[ManoManoAssert\PromotedProductInputValidPlatformIdAndModelId(groups: ['CrossFields'])]
final class PromotedProductInputDTO implements DTOInterface
{
    /**
     * @param string $platformId
     * @param int    $modelId
     * @param int    $position
     */
    public function __construct(
        #[Assert\Choice(callback: [Platform::class, 'casesString'], groups: ['Business'])]
        private readonly mixed $platformId,
        private readonly mixed $modelId,
        #[Assert\GreaterThan(value: 0, groups: ['Business'])]
        private readonly mixed $position,
    ) {
    }

    /**
     * @return string
     */
    public function getPlatformId(): mixed
    {
        return $this->platformId;
    }

    /**
     * @return int
     */
    public function getModelId(): mixed
    {
        return $this->modelId;
    }

    /**
     * @return int
     */
    public function getPosition(): mixed
    {
        return $this->position;
    }
}
