<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use Manomano\ApiSpecs\Pagination\PaginatedContentInterface;
use Manomano\ApiSpecs\Pagination\Pagination;

class ListOutputDTO implements DTOInterface, PaginatedContentInterface
{
    /**
     * @param array<mixed> $items
     */
    public function __construct(
        private readonly array $items,
        private readonly Pagination $pagination,
    ) {
    }

    /**
     * @return array<mixed>
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }
}
