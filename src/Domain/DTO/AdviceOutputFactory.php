<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\Advice;

class AdviceOutputFactory
{
    /**
     * @param array<Advice> $list
     */
    public function createFromAdviceList(array $list): AdviceOutputDTO
    {
        $tipsheetIds = [];
        foreach ($list as $advice) {
            $tipsheetIds[] = $advice->getTipsheetId();
        }

        return new AdviceOutputDTO($tipsheetIds);
    }
}
