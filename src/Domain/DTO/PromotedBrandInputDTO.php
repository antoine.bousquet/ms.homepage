<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\PromotedBrand;
use App\Domain\Enum\Platform;
use App\Domain\Validation\Constraint as ManoManoAssert;
use Symfony\Component\Validator\Constraints as Assert;

#[Assert\GroupSequence(['PromotedBrandInputDTO', 'Business'])]
#[ManoManoAssert\IsDatabaseConsistent(PromotedBrand::class)]
final class PromotedBrandInputDTO implements DTOInterface
{
    /**
     * @param string $platformId
     * @param int    $brandId
     * @param int    $position
     */
    public function __construct(
        #[Assert\Choice(callback: [Platform::class, 'casesString'], groups: ['Business'])]
        private readonly mixed $platformId,
        #[ManoManoAssert\ValidBrandId(groups: ['Business'])]
        private readonly mixed $brandId,
        #[Assert\GreaterThan(value: 0, groups: ['Business'])]
        private readonly mixed $position,
    ) {
    }

    /**
     * @return string
     */
    public function getPlatformId(): mixed
    {
        return $this->platformId;
    }

    /**
     * @return int
     */
    public function getBrandId(): mixed
    {
        return $this->brandId;
    }

    /**
     * @return int
     */
    public function getPosition(): mixed
    {
        return $this->position;
    }
}
