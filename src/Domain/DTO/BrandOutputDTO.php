<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

final class BrandOutputDTO implements DTOInterface
{
    /**
     * @param array<int> $ids
     */
    public function __construct(
        #[Groups(['GET_CONFIGURATION'])]
        private readonly array $ids,
    ) {
    }

    /**
     * @return array<int>
     */
    public function getIds(): array
    {
        return $this->ids;
    }
}
