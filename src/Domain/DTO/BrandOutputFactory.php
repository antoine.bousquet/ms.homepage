<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\PromotedBrand;

class BrandOutputFactory
{
    /**
     * @param array<PromotedBrand> $list
     */
    public function createFromPromotedBrandList(array $list): BrandOutputDTO
    {
        $ids = [];
        foreach ($list as $promotedBrand) {
            $ids[] = $promotedBrand->getBrandId();
        }

        return new BrandOutputDTO($ids);
    }
}
