<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use Symfony\Component\Serializer\Annotation\Groups;

final class AdviceOutputDTO implements DTOInterface
{
    /**
     * @param array<int> $tipsheetIds
     */
    public function __construct(
        #[Groups(['GET_CONFIGURATION'])]
        private readonly array $tipsheetIds,
    ) {
    }

    /**
     * @return array<int>
     */
    public function getTipsheetIds(): array
    {
        return $this->tipsheetIds;
    }
}
