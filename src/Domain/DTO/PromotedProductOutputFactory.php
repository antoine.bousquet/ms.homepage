<?php

declare(strict_types=1);

namespace App\Domain\DTO;

use App\Domain\Entity\Homepage\PromotedProduct;

class PromotedProductOutputFactory
{
    /**
     * @param array<PromotedProduct> $list
     */
    public function createFromPromotedProductList(array $list): PromotedProductOutputDTO
    {
        $promotedProducts = [];
        $modelIds = [];
        foreach ($list as $promotedProduct) {
            if (!\in_array($promotedProduct->getModelId(), $modelIds, true)) {
                $promotedProducts[] = new PromotedProductOutputItemDTO($promotedProduct->getModelId(), $promotedProduct->getPosition());
                $modelIds[] = $promotedProduct->getModelId();
            }
        }

        return new PromotedProductOutputDTO($promotedProducts);
    }
}
