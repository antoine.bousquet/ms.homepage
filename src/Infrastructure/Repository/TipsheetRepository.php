<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Tipsheet;
use App\Infrastructure\Client\CmsClient;

class TipsheetRepository implements TipsheetRepositoryInterface
{
    public function __construct(private readonly CmsClient $cmsHttpClient)
    {
    }

    public function findOneById(int $id): ?Tipsheet
    {
        return $this->cmsHttpClient->getTipsheet($id);
    }
}
