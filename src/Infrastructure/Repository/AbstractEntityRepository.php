<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * @template TEntityClass of object
 *
 * @extends ServiceEntityRepository<TEntityClass>
 */
abstract class AbstractEntityRepository extends ServiceEntityRepository
{
    public function getClassName(): string
    {
        return parent::getClassName();
    }

    public function createPlatformAwareQueryBuilder(string $alias, string $platform): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder($alias);
        $queryBuilder
            ->andWhere("$alias.platform = :platform")
            ->setParameter('platform', $platform);

        return $queryBuilder;
    }

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     * @param positive-int|null          $limit
     * @param positive-int|null          $offset
     *
     * @return array<TEntityClass>|null
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): ?array
    {
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }
}
