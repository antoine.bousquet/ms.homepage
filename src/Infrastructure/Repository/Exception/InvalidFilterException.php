<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Exception;

final class InvalidFilterException extends \InvalidArgumentException
{
    /**
     * @param array<string> $availableFilters
     */
    public function __construct(
        private readonly string $filter,
        private readonly array $availableFilters,
        int $code = 0,
        \Throwable $previous = null,
    ) {
        $message = sprintf(
            'Filter "%s" is invalid. Valid filters: %s',
            $filter,
            implode(', ', $availableFilters)
        );
        parent::__construct($message, $code, $previous);
    }

    public function getFilter(): string
    {
        return $this->filter;
    }

    /**
     * @return array<string>
     */
    public function getAvailableFilters(): array
    {
        return $this->availableFilters;
    }
}
