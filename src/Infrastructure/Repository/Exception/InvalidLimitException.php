<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Exception;

final class InvalidLimitException extends \InvalidArgumentException
{
    public function __construct(int $limit, int $maximumLimit, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf(
            'Limit must be %d or less, %d provided.',
            $maximumLimit,
            $limit
        );
        parent::__construct($message, $code, $previous);
    }
}
