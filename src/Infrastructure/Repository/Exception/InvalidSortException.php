<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Exception;

use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use Webmozart\Assert\Assert;

final class InvalidSortException extends \InvalidArgumentException
{
    /**
     * @param array<string> $availableSorts
     */
    public function __construct(
        private readonly string $sortField,
        ?string $sortOrder,
        private readonly array $availableSorts,
        int $code = 0,
        \Throwable $previous = null,
    ) {
        Assert::nullOrInArray($sortOrder, ListRepositoryInterface::SORT_ORDERS);

        $message = sprintf(
            'Sort field "%s" is invalid%s. Valid sorts: %s',
            $sortField,
            null === $sortOrder ? '' : " on direction '$sortOrder'",
            0 === \count($availableSorts) ? 'there are no valid sorts for this action.' : implode(', ', $availableSorts)
        );
        parent::__construct($message, $code, $previous);
    }

    public function getSortField(): string
    {
        return $this->sortField;
    }

    /**
     * @return array<string>
     */
    public function getAvailableSorts(): array
    {
        return $this->availableSorts;
    }
}
