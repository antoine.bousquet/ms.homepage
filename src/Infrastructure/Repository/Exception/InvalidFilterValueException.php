<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Exception;

final class InvalidFilterValueException extends \InvalidArgumentException
{
    public function __construct(
        private readonly string $messageTemplate,
        private readonly string $filter,
        int $code = 0,
        \Throwable $previous = null,
    ) {
        parent::__construct('', $code, $previous);
    }

    public function getMessageTemplate(): string
    {
        return $this->messageTemplate;
    }

    public function getFilter(): string
    {
        return $this->filter;
    }
}
