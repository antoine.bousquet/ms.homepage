<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Product;

interface ProductRepositoryInterface extends RepositoryInterface
{
    public function findOneByModelIdAndPlatform(int $modelId, string $platformId): ?Product;

    public function isModelIdMarkedDown(int $modelId, string $platformId): bool;
}
