<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\Homepage\AdministrationLog;
use App\Infrastructure\Repository\CRUD\FilterConfiguration;
use App\Infrastructure\Repository\Traits\ListTrait;
use App\Infrastructure\Repository\Traits\SaveTrait;
use Doctrine\Persistence\ManagerRegistry;
use Manomano\SecurityBundle\User\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

/**
 * @extends AbstractEntityRepository<AdministrationLog>
 */
final class AdministrationLogRepository extends AbstractEntityRepository implements AdministrationLogRepositoryInterface
{
    use ListTrait;
    use SaveTrait;

    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        ValidatorInterface $validator,
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, AdministrationLog::class);
        $this->validator = $validator;

        $this->filterConfigurations = [
            new FilterConfiguration('resourceName'),
            new FilterConfiguration('resourceId'),
            new FilterConfiguration('action'),
        ];
        $this->availableSorts = ['dateTime'];
    }

    public function createFromAdministrableResourceEntity(AdministrableResourceEntityInterface $entity, string $action, string $resourceName): AdministrationLog
    {
        Assert::inArray($action, AdministrationLog::ACTIONS);

        $log = new AdministrationLog();
        $log->setAction($action);
        $log->setResourceName($resourceName);
        $log->setResourceId($entity->getId());
        $log->setDateTime(new \DateTimeImmutable());
        $log->setUsername($this->getUsername());

        return $log;
    }

    private function getUsername(): string
    {
        $token = $this->tokenStorage->getToken();
        if (null === $token || !$token->getUser() instanceof User) {
            throw new \LogicException('Only logged-in ManoMano users should be able to administrate content.');
        }

        return $token->getUser()->getUsername();
    }
}
