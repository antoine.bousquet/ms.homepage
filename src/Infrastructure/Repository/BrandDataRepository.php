<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\BrandData;
use App\Domain\Entity\ListResult;
use App\Infrastructure\Client\BrandClient;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\Exception\InvalidFilterException;
use App\Infrastructure\Repository\Exception\InvalidFilterValueException;
use App\Infrastructure\Repository\Exception\InvalidLimitException;
use App\Infrastructure\Repository\Exception\InvalidSortException;
use Webmozart\Assert\Assert;
use Webmozart\Assert\InvalidArgumentException;

final class BrandDataRepository implements BrandDataRepositoryInterface
{
    public function __construct(private readonly BrandClient $client)
    {
    }

    public function findOneById(string $id, string $context = GetRepositoryInterface::CONTEXT_READ): ?BrandData
    {
        return $this->client->getBrandFromLegacyId((int) $id);
    }

    /**
     * {@inheritDoc}
     */
    public function list(array $filters = [], array $sorts = [], int $limit = self::MAXIMUM_LIMIT, int $page = self::DEFAULT_PAGE, string $context = GetRepositoryInterface::CONTEXT_READ): ListResult
    {
        Assert::isMap($filters);
        Assert::isMap($sorts);
        Assert::allInArray($sorts, ListRepositoryInterface::SORT_ORDERS);
        Assert::positiveInteger($page);
        Assert::positiveInteger($limit);

        if (ListRepositoryInterface::MAXIMUM_LIMIT < $limit) {
            throw new InvalidLimitException($limit, ListRepositoryInterface::MAXIMUM_LIMIT);
        }

        // Handling unrecognized filters
        if (\count($filters) > 0) {
            $unknownFilters = array_diff(array_keys($filters), ['id', 'name']);
            if (\count($unknownFilters) > 0) {
                throw new InvalidFilterException(reset($unknownFilters), ['id', 'name']);
            }
        }

        // Handling unrecognized sort parameters
        if (\count($sorts) > 0) {
            $unknownSortFields = array_diff(array_keys($sorts), ['name']);
            if (\count($unknownSortFields) > 0) {
                throw new InvalidSortException(reset($sorts), null, []);
            }
        }

        // Validate brand IDs
        $ids = $filters['id'] ?? null;
        if (null !== $ids) {
            try {
                Assert::isArray($ids);
                Assert::allPositiveInteger($ids);
            } catch (InvalidArgumentException $exception) {
                throw new InvalidFilterValueException('Provided value for filter "{{ filter }}" must be an array of strictly positive integers', 'id', 0, $exception);
            }
        }

        // Validate brand name
        $name = $filters['name'] ?? null;
        if (null !== $name && !\is_string($name)) {
            throw new InvalidFilterValueException('Provided value for filter "{{ filter }}" must be a string', 'name');
        }

        // Validate sorts. The only one allowed is 'name' in 'ASC' order.
        $sortFields = [];
        if (\count($sorts) > 0) {
            if ($sorts !== ['name' => 'ASC']) {
                throw new InvalidSortException('name', 'DESC', ['"name" ("ASC" order only)']);
            }
            $sortFields = ['name'];
        }

        return $this->client->listBrands($ids, $name, $sortFields, $limit, $page);
    }

    public function getClassName(): string
    {
        return BrandData::class;
    }
}
