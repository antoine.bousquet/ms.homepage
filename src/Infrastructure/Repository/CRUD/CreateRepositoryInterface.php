<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use App\Domain\Entity\EntityInterface;
use App\Infrastructure\Repository\RepositoryInterface;

interface CreateRepositoryInterface extends RepositoryInterface
{
    public function createNew(): EntityInterface;

    public function save(EntityInterface $entity): void;

    public function flush(): void;
}
