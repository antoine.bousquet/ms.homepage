<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use App\Domain\Entity\ListResult;
use App\Infrastructure\Repository\RepositoryInterface;

interface ListRepositoryInterface extends RepositoryInterface
{
    public const SORT_ORDER_ASC = 'ASC';
    public const SORT_ORDER_DESC = 'DESC';
    public const SORT_ORDERS = ['ASC', 'DESC'];

    public const MAXIMUM_LIMIT = 1000;
    public const DEFAULT_PAGE = 1;

    /**
     * @param array<string, mixed>  $filters
     * @param array<string, string> $sorts
     * @param positive-int          $limit
     * @param positive-int          $page
     */
    public function list(array $filters = [], array $sorts = [], int $limit = self::MAXIMUM_LIMIT, int $page = self::DEFAULT_PAGE, string $context = GetRepositoryInterface::CONTEXT_READ): ListResult;
}
