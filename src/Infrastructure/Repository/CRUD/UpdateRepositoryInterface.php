<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use App\Domain\Entity\EntityInterface;

interface UpdateRepositoryInterface extends GetRepositoryInterface
{
    public function save(EntityInterface $entity): void;

    public function flush(): void;
}
