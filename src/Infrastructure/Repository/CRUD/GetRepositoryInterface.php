<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use App\Domain\Entity\EntityInterface;
use App\Infrastructure\Repository\RepositoryInterface;

interface GetRepositoryInterface extends RepositoryInterface
{
    public const CONTEXT_READ = 'READ';
    public const CONTEXT_WRITE = 'WRITE';
    public const CONTEXTS = [
        self::CONTEXT_READ,
        self::CONTEXT_WRITE,
    ];

    public function findOneById(string $id, string $context = self::CONTEXT_READ): ?EntityInterface;

    public function getClassName(): string;
}
