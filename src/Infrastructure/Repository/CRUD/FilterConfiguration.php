<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use Webmozart\Assert\Assert;

class FilterConfiguration
{
    final public const NOT_MULTIPLE = 'NOT_MULTIPLE';
    final public const ALLOW_MULTIPLE = 'ALLOW_MULTIPLE';
    final public const ONLY_MULTIPLE = 'ONLY_MULTIPLE';
    final public const MULTIPLICITIES = [
        self::NOT_MULTIPLE,
        self::ALLOW_MULTIPLE,
        self::ONLY_MULTIPLE,
    ];

    public function __construct(
        private readonly string $name,
        private readonly string $multiplicity = self::NOT_MULTIPLE,
    ) {
        Assert::inArray($multiplicity, self::MULTIPLICITIES);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function allowMultipleValues(): bool
    {
        return self::NOT_MULTIPLE !== $this->multiplicity;
    }

    public function allowSimpleValue(): bool
    {
        return self::ONLY_MULTIPLE !== $this->multiplicity;
    }
}
