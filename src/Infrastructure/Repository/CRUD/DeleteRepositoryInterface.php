<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\CRUD;

use App\Domain\Entity\EntityInterface;

interface DeleteRepositoryInterface extends GetRepositoryInterface
{
    public function delete(EntityInterface $entity): void;

    public function flush(): void;
}
