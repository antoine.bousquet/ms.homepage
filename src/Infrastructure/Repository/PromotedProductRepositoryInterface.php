<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Homepage\PromotedProduct;
use App\Infrastructure\Repository\CRUD\CreateRepositoryInterface;
use App\Infrastructure\Repository\CRUD\DeleteRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\CRUD\UpdateRepositoryInterface;

interface PromotedProductRepositoryInterface extends CreateRepositoryInterface, UpdateRepositoryInterface, DeleteRepositoryInterface, ListRepositoryInterface
{
    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<PromotedProduct>|null
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): ?array;
}
