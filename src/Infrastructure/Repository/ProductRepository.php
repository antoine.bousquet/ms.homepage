<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Product;
use App\Infrastructure\Client\RhinoReaderClient;

class ProductRepository implements ProductRepositoryInterface
{
    public function __construct(private readonly RhinoReaderClient $rhinoReaderHttpClient)
    {
    }

    public function findOneByModelIdAndPlatform(int $modelId, string $platformId): ?Product
    {
        static $products = [];

        if (!isset($products[$platformId][$modelId])) {
            $products[$platformId][$modelId] = $this->rhinoReaderHttpClient->getProduct($platformId, $modelId);
        }

        return $products[$platformId][$modelId];
    }

    public function isModelIdMarkedDown(int $modelId, string $platformId): bool
    {
        $product = $this->findOneByModelIdAndPlatform($modelId, $platformId);

        return
            null !== $product
            && null !== $product->getDiscountPercentage()
            && \is_int($product->getDiscountPercentage()) && $product->getDiscountPercentage() > 0;
    }
}
