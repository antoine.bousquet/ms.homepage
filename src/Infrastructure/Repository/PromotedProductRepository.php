<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Homepage\PromotedProduct;
use App\Infrastructure\Repository\CRUD\FilterConfiguration;
use App\Infrastructure\Repository\Traits\CreateTrait;
use App\Infrastructure\Repository\Traits\DeleteTrait;
use App\Infrastructure\Repository\Traits\GetTrait;
use App\Infrastructure\Repository\Traits\ListTrait;
use App\Infrastructure\Repository\Traits\UpdateTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @extends AbstractEntityRepository<PromotedProduct>
 */
final class PromotedProductRepository extends AbstractEntityRepository implements PromotedProductRepositoryInterface
{
    use CreateTrait;
    use DeleteTrait;
    use GetTrait;
    use ListTrait;
    use UpdateTrait;

    public function __construct(ValidatorInterface $validator, ManagerRegistry $registry)
    {
        parent::__construct($registry, PromotedProduct::class);
        $this->validator = $validator;
        $this->setEntityManagerFromRegistry($registry);
        $this->filterConfigurations = [
            new FilterConfiguration('modelId'),
            new FilterConfiguration('platformId'),
        ];
        $this->availableSorts = [
            'modelId',
            'platformId',
            'position',
        ];
    }
}
