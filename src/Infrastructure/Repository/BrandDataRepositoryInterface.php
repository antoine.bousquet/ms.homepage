<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;

interface BrandDataRepositoryInterface extends ListRepositoryInterface, GetRepositoryInterface
{
}
