<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Webmozart\Assert\Assert;

trait ReadOnlyEntityManagerTrait
{
    private EntityManagerInterface $readOnlyEntityManager;

    private function setEntityManagerFromRegistry(ManagerRegistry $registry): void
    {
        $entityManager = $registry->getManager('read_only');
        if (!$entityManager instanceof EntityManagerInterface) {
            throw new \LogicException(sprintf('The read-only entity manager must be an instance of "%s", "%s" given', EntityManagerInterface::class, \get_class($entityManager)));
        }
        $this->readOnlyEntityManager = $entityManager;
    }

    private function getContextEntityManager(string $context): EntityManagerInterface
    {
        Assert::oneOf($context, GetRepositoryInterface::CONTEXTS);

        return
            GetRepositoryInterface::CONTEXT_WRITE === $context
            ? $this->_em
            : $this->readOnlyEntityManager;
    }
}
