<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\ListResult;
use App\Infrastructure\Repository\CRUD\FilterConfiguration;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\Exception\InvalidFilterException;
use App\Infrastructure\Repository\Exception\InvalidFilterValueException;
use App\Infrastructure\Repository\Exception\InvalidLimitException;
use App\Infrastructure\Repository\Exception\InvalidSortException;
use App\Tests\UnitTests\Tests\Infrastructure\Repository\Traits\Mock\Paginator as MockPaginator;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Validator\Constraints\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

trait ListTrait
{
    use ReadOnlyEntityManagerTrait;

    /**
     * Set as a protected string to make unit testing easier.
     *
     * @var class-string
     */
    protected string $paginatorClassName = Paginator::class;

    private readonly ValidatorInterface $validator;

    /**
     * @var array<FilterConfiguration>
     */
    private readonly array $filterConfigurations;

    /**
     * @var array<string>
     */
    private readonly array $availableSorts;

    /**
     * @param array<string, mixed>  $filters
     * @param array<string, string> $sorts
     * @param positive-int          $limit
     * @param positive-int          $page
     */
    public function list(
        array $filters = [],
        array $sorts = [],
        int $limit = ListRepositoryInterface::MAXIMUM_LIMIT,
        int $page = ListRepositoryInterface::DEFAULT_PAGE,
        string $context = GetRepositoryInterface::CONTEXT_READ
    ): ListResult {
        Assert::isMap($filters);
        Assert::isMap($sorts);
        Assert::allInArray($sorts, ListRepositoryInterface::SORT_ORDERS);
        Assert::positiveInteger($page);
        Assert::positiveInteger($limit);

        if (ListRepositoryInterface::MAXIMUM_LIMIT < $limit) {
            throw new InvalidLimitException($limit, ListRepositoryInterface::MAXIMUM_LIMIT);
        }

        $entityManager = $this->getContextEntityManager($context);
        $queryBuilder = $entityManager->createQueryBuilder()
            ->select('t')
            ->from($this->getClassName(), 't')
            ->setMaxResults($limit)
            ->setFirstResult($limit * ($page - 1));

        $this->handleListQueryBuilder($queryBuilder);

        foreach ($filters as $field => $value) {
            if (null === ($filterConfiguration = $this->getFilterConfiguration($field))) {
                $validFilterNames = array_map(static function (FilterConfiguration $filterConfiguration): string {
                    return '"'.$filterConfiguration->getName().'"';
                }, $this->filterConfigurations);

                throw new InvalidFilterException($field, $validFilterNames);
            }

            $this->validateFilterValue($filterConfiguration, $value);

            $parameterName = "filter_$field";
            if (\is_scalar($value)) {
                $queryBuilder->andWhere("t.$field = :$parameterName");
            } else {
                $queryBuilder->andWhere("t.$field IN (:$parameterName)");
            }
            $queryBuilder->setParameter($parameterName, $value);
        }

        foreach ($sorts as $field => $order) {
            if (!\in_array($field, $this->availableSorts, true)) {
                throw new InvalidSortException($field, null, $this->availableSorts);
            }

            $queryBuilder->addOrderBy("t.$field", $order);
        }

        $className = $this->paginatorClassName;

        /**
         * @var Paginator|MockPaginator $paginator
         */
        $paginator = new $className($queryBuilder->getQuery());

        /**
         * @var positive-int $count
         */
        $count = $paginator->count();

        /**
         * @var iterable<EntityInterface> $result
         */
        $result = $paginator->getQuery()->getResult();

        return new ListResult($result, $count);
    }

    protected function handleListQueryBuilder(QueryBuilder $queryBuilder): void
    {
    }

    private function getFilterConfiguration(string $name): ?FilterConfiguration
    {
        foreach ($this->filterConfigurations as $filterConfiguration) {
            if ($name === $filterConfiguration->getName()) {
                return $filterConfiguration;
            }
        }

        return null;
    }

    /**
     * @param mixed|array<mixed> $value
     */
    private function validateFilterValue(FilterConfiguration $filterConfiguration, $value): void
    {
        $metadata = $this->getClassMetadata();
        $type = $metadata->getTypeOfField($filterConfiguration->getName());

        switch ($type) {
            case Types::INTEGER:
                $this->doValidateFilterValue($filterConfiguration, $value, 'integer', static function ($value): bool {
                    return \is_int($value);
                });
                break;
            case Types::STRING:
                $this->doValidateFilterValue($filterConfiguration, $value, 'string', static function ($value): bool {
                    return \is_string($value);
                });
                break;
            case Types::GUID:
                $this->doValidateFilterValue($filterConfiguration, $value, 'uuid', function ($value): bool {
                    $violations = $this->validator->validate($value, new Uuid());

                    return 0 === \count($violations);
                });
                break;
            default:
                throw new \LogicException(sprintf('Type of provided filter "%s" is "%s", which is not yet handled automatically.', $filterConfiguration->getName(), $type));
        }
    }

    /**
     * @param mixed|array<mixed> $value
     */
    private function doValidateFilterValue(FilterConfiguration $filterConfiguration, $value, string $typeLabel, callable $validate): void
    {
        if (
            !($filterConfiguration->allowSimpleValue() && $validate($value)) &&
            !($filterConfiguration->allowMultipleValues() && \is_array($value) && $value === array_filter($value, $validate))
        ) {
            $allowed = [];
            if ($filterConfiguration->allowSimpleValue()) {
                $allowed[] = 'a "'.$typeLabel.'"';
            }
            if ($filterConfiguration->allowMultipleValues()) {
                $allowed[] = 'an array of "'.$typeLabel.'"';
            }

            throw new InvalidFilterValueException(sprintf('Provided filter value for "{{ filter }}" must be %s', implode(' or ', $allowed)), $filterConfiguration->getName());
        }
    }
}
