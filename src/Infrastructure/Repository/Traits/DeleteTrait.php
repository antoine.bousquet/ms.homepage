<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Domain\Entity\EntityInterface;
use Webmozart\Assert\Assert;

trait DeleteTrait
{
    use FlushTrait;

    public function delete(EntityInterface $entity): void
    {
        /**
         * @var class-string<EntityInterface> $className
         */
        $className = $this->getClassName();
        Assert::isInstanceOf($entity, $className);
        $this->_em->remove($entity);
    }
}
