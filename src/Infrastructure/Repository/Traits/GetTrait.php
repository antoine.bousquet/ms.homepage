<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Domain\Entity\EntityInterface;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;

trait GetTrait
{
    use ReadOnlyEntityManagerTrait;

    public function findOneById(string $id, string $context = GetRepositoryInterface::CONTEXT_READ): ?EntityInterface
    {
        /**
         * @var class-string<EntityInterface> $className
         */
        $className = $this->getClassName();
        $entityManager = $this->getContextEntityManager($context);

        return $entityManager->find($className, $id);
    }
}
