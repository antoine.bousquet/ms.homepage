<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Domain\Entity\EntityInterface;
use Doctrine\ORM\EntityRepository;
use Webmozart\Assert\Assert;

trait SaveTrait
{
    use FlushTrait;

    public function save(EntityInterface $entity): void
    {
        if (!$this instanceof EntityRepository) {
            throw new \LogicException(sprintf('The SaveTrait trait can only be applied to a class extending "%s"', EntityRepository::class));
        }

        /**
         * @var class-string<EntityInterface> $className
         */
        $className = $this->getClassName();
        Assert::isInstanceOf($entity, $className);
        $this->_em->persist($entity);
    }
}
