<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use App\Domain\Entity\EntityInterface;

trait CreateTrait
{
    use SaveTrait;

    public function createNew(): EntityInterface
    {
        $className = $this->getClassName();

        /**
         * @var EntityInterface $entity
         */
        $entity = new $className();

        return $entity;
    }
}
