<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

trait UpdateTrait
{
    use SaveTrait;
}
