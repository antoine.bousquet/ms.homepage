<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Traits;

use Doctrine\ORM\EntityRepository;

trait FlushTrait
{
    public function flush(): void
    {
        if (!$this instanceof EntityRepository) {
            throw new \LogicException(sprintf('The SaveTrait trait can only be applied to a class extending "%s"', EntityRepository::class));
        }

        $this->_em->flush();
    }
}
