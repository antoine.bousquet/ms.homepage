<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Homepage\Advice;
use App\Infrastructure\Repository\CRUD\FilterConfiguration;
use App\Infrastructure\Repository\Traits\CreateTrait;
use App\Infrastructure\Repository\Traits\DeleteTrait;
use App\Infrastructure\Repository\Traits\GetTrait;
use App\Infrastructure\Repository\Traits\ListTrait;
use App\Infrastructure\Repository\Traits\UpdateTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @extends AbstractEntityRepository<Advice>
 */
final class AdviceRepository extends AbstractEntityRepository implements AdviceRepositoryInterface
{
    use CreateTrait;
    use DeleteTrait;
    use GetTrait;
    use ListTrait;
    use UpdateTrait;

    public function __construct(ValidatorInterface $validator, ManagerRegistry $registry)
    {
        parent::__construct($registry, Advice::class);
        $this->validator = $validator;
        $this->setEntityManagerFromRegistry($registry);
        $this->filterConfigurations = [
            new FilterConfiguration('platformId'),
            new FilterConfiguration('tipsheetId'),
        ];
        $this->availableSorts = [
            'platformId',
            'tipsheetId',
            'position',
        ];
    }
}
