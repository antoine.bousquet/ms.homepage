<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Tipsheet;

interface TipsheetRepositoryInterface extends RepositoryInterface
{
    public function findOneById(int $id): ?Tipsheet;
}
