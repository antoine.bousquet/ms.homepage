<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\Homepage\AdministrationLog;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;

interface AdministrationLogRepositoryInterface extends ListRepositoryInterface
{
    public function createFromAdministrableResourceEntity(AdministrableResourceEntityInterface $entity, string $action, string $resourceName): AdministrationLog;

    public function save(EntityInterface $entity): void;
}
