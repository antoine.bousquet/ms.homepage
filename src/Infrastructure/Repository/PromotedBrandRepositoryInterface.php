<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Homepage\PromotedBrand;
use App\Infrastructure\Repository\CRUD\CreateRepositoryInterface;
use App\Infrastructure\Repository\CRUD\DeleteRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\CRUD\UpdateRepositoryInterface;

interface PromotedBrandRepositoryInterface extends ListRepositoryInterface, CreateRepositoryInterface, UpdateRepositoryInterface, DeleteRepositoryInterface
{
    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<PromotedBrand>|null
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): ?array;
}
