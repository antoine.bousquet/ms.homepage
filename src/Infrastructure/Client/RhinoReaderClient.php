<?php

declare(strict_types=1);

namespace App\Infrastructure\Client;

use App\Domain\Entity\Product;
use App\Domain\Enum\Platform;
use App\Infrastructure\Client\Exception\InvalidClientResponseFormatException;
use App\Infrastructure\Client\Exception\RhinoReaderUnavailableException;
use Psr\Http\Client\NetworkExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class RhinoReaderClient extends AbstractManoManoMicroServiceClient
{
    public function getProduct(string $platformId, int $modelId): ?Product
    {
        Assert::inArray($platformId, Platform::casesString()); // @phpstan-ignore-line
        Assert::positiveInteger($modelId);

        $uri = '/api/v2/product-discovery/products?'.http_build_query([
            'model_id' => $modelId,
            'is_pro' => false,
        ]);

        try {
            $response = $this->doRequest(
                Request::METHOD_GET,
                $uri,
                [
                    'x-platform' => $platformId,
                    'Content-Type' => 'application/json',
                ]
            );
        } catch (NetworkExceptionInterface $exception) {
            throw new RhinoReaderUnavailableException($uri);
        }

        if ($this->isNotFound($response) || $this->isBadRequest($response)) {
            return null;
        }
        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new RhinoReaderUnavailableException($uri, (string) $response->getStatusCode());
        }

        /**
         * @var array{content?: array{
         *     offer?: array{price?: array{discount_percentage_for_primary_price?: positive-int}},
         *     sku?: array{model_id?: positive-int},
         * }} $content
         */
        $content = json_decode($response->getBody()->getContents(), true);
        if (!\array_key_exists('content', $content)) {
            $this->logger->error(
                self::class.' error denormalizing Product',
                [
                    'class' => self::class.':'.__METHOD__,
                    'content' => $content,
                    'model_id' => $modelId,
                ]
            );

            throw new InvalidClientResponseFormatException(self::class, $uri);
        }

        $content = $content['content'];

        $discount = $content['offer']['price']['discount_percentage_for_primary_price'] ?? null;
        $discount = (null === $discount ? null : (int) $discount);

        $modelId = (int) ($content['sku']['model_id'] ?? $modelId);

        return new Product(
            $modelId,
            $platformId,
            $discount
        );
    }
}
