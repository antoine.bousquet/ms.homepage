<?php

declare(strict_types=1);

namespace App\Infrastructure\Client\Exception;

final class InvalidClientResponseFormatException extends \RuntimeException
{
    public function __construct(string $client, string $uri, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf(
            'Invalid response format for %s from URL %s',
            $client,
            $uri
        );

        parent::__construct($message, $code, $previous);
    }
}
