<?php

declare(strict_types=1);

namespace App\Infrastructure\Client\Exception;

final class BrandUnavailableException extends \RuntimeException
{
    public function __construct(string $url, ?string $httpStatusCode = null, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf('ms.brand could not be reached on URL "%s"', $url);
        if (null !== $httpStatusCode) {
            $message .= sprintf(' - HTTP status code "%s".', $httpStatusCode);
        }

        parent::__construct($message, $code, $previous);
    }
}
