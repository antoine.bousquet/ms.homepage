<?php

declare(strict_types=1);

namespace App\Infrastructure\Client;

use Manomano\ClientBundle\Client\AbstractMicroserviceClient;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractManoManoMicroServiceClient extends AbstractMicroserviceClient
{
    private const RESPONSE_ERROR_APP_CODE_BAD_REQUEST = 'BAD_REQUEST';

    protected function isNotFound(ResponseInterface $response): bool
    {
        return Response::HTTP_NOT_FOUND === $response->getStatusCode();
    }

    protected function isBadRequest(ResponseInterface $response): bool
    {
        return
            Response::HTTP_BAD_REQUEST === $response->getStatusCode()
            && self::RESPONSE_ERROR_APP_CODE_BAD_REQUEST === $this->extractAppCodeFromResponse($response);
    }

    private function extractFromResponse(ResponseInterface $response, string $key): ?string
    {
        $body = json_decode($response->getBody()->getContents(), true);
        if (!\is_array($body)) {
            return null;
        }

        return $body[$key] ?? null;
    }

    private function extractAppCodeFromResponse(ResponseInterface $response): ?string
    {
        return $this->extractFromResponse($response, 'app_code');
    }
}
