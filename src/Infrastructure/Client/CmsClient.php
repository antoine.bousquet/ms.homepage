<?php

declare(strict_types=1);

namespace App\Infrastructure\Client;

use App\Domain\Entity\Tipsheet;
use App\Infrastructure\Client\Exception\CmsUnavailableException;
use App\Infrastructure\Client\Exception\InvalidClientResponseFormatException;
use Http\Message\MessageFactory;
use Psr\Http\Client\NetworkExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Webmozart\Assert\Assert;

class CmsClient extends AbstractManoManoMicroServiceClient
{
    private const TIPSHEET_ENDPOINT = '/api/v2/tipsheet/tipsheets/%d';

    private DenormalizerInterface $serializer;

    public function __construct($httpClient, MessageFactory $messageFactory, DenormalizerInterface $serializer)
    {
        parent::__construct($httpClient, $messageFactory);

        $this->serializer = $serializer;
    }

    public function getTipsheet(int $id): ?Tipsheet
    {
        Assert::positiveInteger($id);

        $uri = sprintf(self::TIPSHEET_ENDPOINT, $id);

        try {
            $response = $this->doRequest(
                Request::METHOD_GET,
                $uri,
                [
                    'Content-Type' => 'application/json',
                ]
            );
        } catch (NetworkExceptionInterface $exception) {
            throw new CmsUnavailableException($uri);
        }

        if ($this->isBadRequest($response)) {
            return null;
        }
        if ($this->isNotFound($response)) {
            throw new NotFoundHttpException();
        }
        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new CmsUnavailableException($uri, (string) $response->getStatusCode());
        }

        $content = json_decode($response->getBody()->getContents(), true);

        try {
            return $this->serializer->denormalize(
                $content,
                Tipsheet::class,
            );
        } catch (\Throwable $exception) {
            $this->logger->error(
                self::class.' error denormalizing Tipsheet ['.$exception->getMessage().']',
                [
                    'class' => self::class.':'.__METHOD__,
                    'error_message' => $exception->getMessage(),
                    'content' => $content,
                    'tipsheet_id' => $id,
                    'exception' => $exception,
                ]
            );

            throw new InvalidClientResponseFormatException(self::class, $uri);
        }
    }
}
