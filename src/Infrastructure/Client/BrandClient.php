<?php

declare(strict_types=1);

namespace App\Infrastructure\Client;

use App\Domain\Entity\BrandData;
use App\Domain\Entity\ListResult;
use App\Infrastructure\Client\Exception\BrandUnavailableException;
use App\Infrastructure\Client\Exception\InvalidClientResponseFormatException;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\Exception\InvalidSortException;
use Psr\Http\Client\NetworkExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class BrandClient extends AbstractManoManoMicroServiceClient
{
    final public const HOMEPAGE_B2C_TO_BRAND_FIELD_MAPPING = [
        'name' => 'title',
    ];

    public function getBrandFromLegacyId(int $id): ?BrandData
    {
        Assert::positiveInteger($id);

        $uri = '/api/v1/brand/brands/id/'.$id;

        try {
            $response = $this->doRequest(
                Request::METHOD_GET,
                $uri
            );
        } catch (NetworkExceptionInterface $exception) {
            throw new BrandUnavailableException($uri);
        }

        if ($this->isNotFound($response)) {
            return null;
        }
        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new BrandUnavailableException($uri, (string) $response->getStatusCode());
        }

        /**
         * @var array{content?: array{brand_id: positive-int, title: string, logo: string}} $content
         */
        $content = json_decode($response->getBody()->getContents(), true);
        if (!\array_key_exists('content', $content)) {
            $this->logger->error(
                self::class.' error denormalizing BrandDTO',
                [
                    'class' => self::class.':'.__METHOD__,
                    'content' => $content,
                    'id' => $id,
                ]
            );

            throw new InvalidClientResponseFormatException(self::class, $uri);
        }

        return $this->rawToModel($content['content']);
    }

    /**
     * @param array<positive-int>|null $ids
     * @param array<string>            $sortFields
     * @param positive-int             $limit
     * @param positive-int             $page
     */
    public function listBrands(?array $ids = null, ?string $name = null, array $sortFields = [], int $limit = ListRepositoryInterface::MAXIMUM_LIMIT, int $page = ListRepositoryInterface::DEFAULT_PAGE): ListResult
    {
        Assert::lessThanEq($limit, ListRepositoryInterface::MAXIMUM_LIMIT);
        Assert::positiveInteger($limit);
        Assert::positiveInteger($page);

        $queryParameters = [
            'hasImage' => 'true',
            'limit' => $limit,
            'page' => $page - 1,
        ];

        if (null !== $ids) {
            Assert::allPositiveInteger($ids);
            $queryParameters['brandIds'] = implode(',', $ids);
        }
        if (null !== $name) {
            $queryParameters['title'] = $name;
        }
        if ([] !== $sortFields) {
            Assert::allString($sortFields);
            $sortFields = array_map(static function (string $homepageB2cName): string {
                if (!isset(self::HOMEPAGE_B2C_TO_BRAND_FIELD_MAPPING[$homepageB2cName])) {
                    throw new InvalidSortException($homepageB2cName, null, array_keys(self::HOMEPAGE_B2C_TO_BRAND_FIELD_MAPPING));
                }

                return self::HOMEPAGE_B2C_TO_BRAND_FIELD_MAPPING[$homepageB2cName];
            }, $sortFields);
            $queryParameters['sort'] = implode(',', $sortFields);
        }

        $uri = '/api/v1/brand/brands?'.http_build_query($queryParameters);

        try {
            $response = $this->doRequest(
                Request::METHOD_GET,
                $uri
            );
        } catch (NetworkExceptionInterface $exception) {
            throw new BrandUnavailableException($uri);
        }

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new BrandUnavailableException($uri, (string) $response->getStatusCode());
        }

        try {
            /**
             * @var array{
             *     content?: array<array{brand_id: positive-int, title: string, logo: string}>,
             *     pagination: array{items: int},
             * } $content
             */
            $content = json_decode($response->getBody()->getContents(), true);
            $brands = $content['content'] ?? [];
            $brands = array_map([$this, 'rawToModel'], $brands);
            $count = $content['pagination']['items'];
        } catch (\Throwable $throwable) {
            throw new InvalidClientResponseFormatException(self::class, $uri, 0, $throwable);
        }

        return new ListResult(
            $brands,
            $count
        );
    }

    /**
     * @param array{brand_id: positive-int, title: string, logo: string} $brand
     */
    private function rawToModel(array $brand): BrandData
    {
        return new BrandData(
            $brand['brand_id'], // @todo Deprecated field
            $brand['title'],
            $brand['logo']
        );
    }
}
