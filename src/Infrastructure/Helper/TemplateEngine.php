<?php

declare(strict_types=1);

namespace App\Infrastructure\Helper;

use Webmozart\Assert\Assert;

class TemplateEngine
{
    /**
     * @var array<string, string>
     */
    private array $defaultParameters = [];

    public function clearDefaultParameters(): void
    {
        $this->setDefaultParameters([]);
    }

    public function addDefaultParameter(string $key, string $value): void
    {
        $this->addDefaultParameters([$key => $value]);
    }

    /**
     * @param array<string, string> $parameters
     */
    public function addDefaultParameters(array $parameters): void
    {
        $this->validateParameters($parameters);
        $this->defaultParameters = [...$this->defaultParameters, ...$parameters];
    }

    /**
     * @param array<string, string> $parameters
     */
    public function setDefaultParameters(array $parameters): void
    {
        $this->validateParameters($parameters);
        $this->defaultParameters = $parameters;
    }

    /**
     * @param array<string, string> $parameters
     */
    public function render(string $template, array $parameters = []): string
    {
        $this->validateParameters($parameters);
        $parameters = [...$this->defaultParameters, ...$parameters];

        foreach ($parameters as $key => $value) {
            $template = str_replace("$key", $value, $template);
        }

        return $template;
    }

    /**
     * @param array<mixed|string>|array<string, mixed|string> $parameters
     */
    private function validateParameters(array $parameters): void
    {
        Assert::isMap($parameters);
        foreach ($parameters as $key => $value) {
            Assert::string($key); // @phpstan-ignore-line
            Assert::string($value);
        }
    }
}
