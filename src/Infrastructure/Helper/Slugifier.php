<?php

declare(strict_types=1);

namespace App\Infrastructure\Helper;

use function Symfony\Component\String\u;

class Slugifier
{
    public function transform(string $str, string $separator = '-'): string
    {
        $str = u($str)
            ->ascii([])
            ->lower()
            ->replaceMatches('/[^a-z0-9\/_|+ -]/', '')
            ->replaceMatches('/[\/_|+ -]+/', $separator)
            ->trim($separator)
            ->toString();

        if (0 === mb_strlen($str)) {
            return $separator;
        }

        return $str;
    }
}
