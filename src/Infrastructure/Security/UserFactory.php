<?php

declare(strict_types=1);

namespace App\Infrastructure\Security;

use Manomano\SecurityBundle\User\Model\User;
use Manomano\SecurityBundle\User\UserFactory as BaseFactory;
use Symfony\Component\Security\Core\User\UserInterface;

class UserFactory extends BaseFactory
{
    private const ROLE_PREFIX = 'ROLE_';

    private const ROLE_PROMOTED_PRODUCT_READER = 'ROLE_PROMOTED_PRODUCT_READER';
    private const ROLE_PROMOTED_PRODUCT_WRITER = 'ROLE_PROMOTED_PRODUCT_WRITER';
    private const ROLE_PROMOTED_BRAND_READER = 'ROLE_PROMOTED_BRAND_READER';
    private const ROLE_PROMOTED_BRAND_WRITER = 'ROLE_PROMOTED_BRAND_WRITER';
    private const ROLE_ADVICE_READER = 'ROLE_ADVICE_READER';
    private const ROLE_ADVICE_WRITER = 'ROLE_ADVICE_WRITER';
    private const ROLE_BRAND_DATA_READER = 'ROLE_BRAND_DATA_READER';

    private const ROLE_SSO_PROMOTED_PRODUCT_READER = '#b2c-retention/homepage-b2c/promoted-product/reader';
    private const ROLE_SSO_PROMOTED_PRODUCT_WRITER = '#b2c-retention/homepage-b2c/promoted-product/writer';
    private const ROLE_SSO_PROMOTED_BRAND_READER = '#b2c-retention/homepage-b2c/promoted-brand/reader';
    private const ROLE_SSO_PROMOTED_BRAND_WRITER = '#b2c-retention/homepage-b2c/promoted-brand/writer';
    private const ROLE_SSO_ADVICE_READER = '#b2c-retention/homepage-b2c/advice/reader';
    private const ROLE_SSO_ADVICE_WRITER = '#b2c-retention/homepage-b2c/advice/writer';
    private const ROLE_SSO_BRAND_DATA_READER = '#b2c-retention/homepage-b2c/brand-data/reader';

    private const ROLE_MAPPING = [
        self::ROLE_SSO_PROMOTED_PRODUCT_READER => self::ROLE_PROMOTED_PRODUCT_READER,
        self::ROLE_SSO_PROMOTED_PRODUCT_WRITER => self::ROLE_PROMOTED_PRODUCT_WRITER,
        self::ROLE_SSO_PROMOTED_BRAND_READER => self::ROLE_PROMOTED_BRAND_READER,
        self::ROLE_SSO_PROMOTED_BRAND_WRITER => self::ROLE_PROMOTED_BRAND_WRITER,
        self::ROLE_SSO_ADVICE_READER => self::ROLE_ADVICE_READER,
        self::ROLE_SSO_ADVICE_WRITER => self::ROLE_ADVICE_WRITER,
        self::ROLE_SSO_BRAND_DATA_READER => self::ROLE_BRAND_DATA_READER,
    ];

    /**
     * @param array{preferred_username: string, realm_access: array{roles: array<string>}} $decodedToken
     */
    public function createUserFromDecodedToken(array $decodedToken): UserInterface
    {
        $roles = [];
        foreach ($decodedToken['realm_access']['roles'] as $role) {
            $role = self::ROLE_MAPPING[$role] ?? $role;
            if (str_starts_with($role, self::ROLE_PREFIX)) {
                $roles[] = $role;
            }
        }

        return new User(
            $decodedToken['preferred_username'],
            $roles,
        );
    }
}
