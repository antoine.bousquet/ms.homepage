<?php

declare(strict_types=1);

namespace App\Application\GraphQL;

use Apollo\Federation\FederatedSchema;

final class Schema extends FederatedSchema
{
    public function __construct(LegacyQueryDecorator $query)
    {
        parent::__construct(['query' => $query]);
    }
}
