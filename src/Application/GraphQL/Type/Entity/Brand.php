<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use Apollo\Federation\Types\EntityRefObjectType;
use App\Application\GraphQL\Type\Enum\Market;
use App\Application\GraphQL\Type\Enum\Platform;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\Type;

final class Brand extends EntityRefObjectType
{
    public const TYPE_NAME = 'Brand';

    public const FIELD_ID = 'brandId';
    public const FIELD_PLATFORM = 'platform';
    public const FIELD_MARKET = 'market';

    public const KEY_FIELDS = [
        self::FIELD_ID,
        self::FIELD_PLATFORM,
        self::FIELD_MARKET,
    ];

    public function __construct(Platform $platformType, Market $marketType)
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'keyFields' => [implode(' ', self::KEY_FIELDS)],
            'description' => 'Brand data',
            'fields' => [
                self::FIELD_ID => [
                    'description' => 'Brand identifier',
                    'type' => new NonNull(Type::int()),
                    'isExternal' => true,
                ],
                self::FIELD_PLATFORM => [
                    'description' => 'Associated platform',
                    'type' => new NonNull($platformType),
                    'isExternal' => true,
                ],
                self::FIELD_MARKET => [
                    'description' => 'Associated market',
                    'type' => new NonNull($marketType),
                    'isExternal' => true,
                ],
            ],
        ]);
    }
}
