<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use Apollo\Federation\Types\EntityRefObjectType;
use App\Application\GraphQL\Type\Enum\Platform;
use App\Application\GraphQL\Type\Scalar\BigInteger;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\Type;

final class ProductCollection extends EntityRefObjectType
{
    public const TYPE_NAME = 'ProductCollection';

    public const FIELD_PLATFORM = 'platform';
    public const FIELD_IS_PRO = 'isPro';
    public const FIELD_MODEL_IDS = 'modelIds';
    public const FIELD_LIMIT = 'limit';

    public const KEY_FIELDS = [
        self::FIELD_PLATFORM,
        self::FIELD_IS_PRO,
        self::FIELD_MODEL_IDS,
        self::FIELD_LIMIT,
    ];

    public function __construct(Platform $platformType, BigInteger $bigIntegerType)
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'keyFields' => [implode(' ', self::KEY_FIELDS)],
            'description' => 'Product collection external entity',
            'fields' => [
                self::FIELD_PLATFORM => [
                    'description' => 'Associated platform',
                    'type' => new NonNull($platformType),
                    'isExternal' => true,
                ],
                self::FIELD_IS_PRO => [
                    'description' => 'Whether these products are reserved to professional accounts',
                    'type' => new NonNull(Type::boolean()),
                    'isExternal' => true,
                ],
                self::FIELD_MODEL_IDS => [
                    'description' => 'Identifiers of the searched models',
                    'type' => new NonNull(new ListOfType(new NonNull($bigIntegerType))),
                ],
                self::FIELD_LIMIT => [
                    'description' => 'Maximum number of products to fetch',
                    'type' => new NonNull(Type::int()),
                ],
            ],
        ]);
    }
}
