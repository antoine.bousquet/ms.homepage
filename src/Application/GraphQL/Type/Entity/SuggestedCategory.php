<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

final class SuggestedCategory extends ObjectType
{
    public const TYPE_NAME = 'SuggestedCategory';

    public const FIELD_ID = 'id';
    public const FIELD_FAMILY_ID = 'familyId';
    public const FIELD_NAME = 'name';
    public const FIELD_IMAGE = 'image';
    public const FIELD_URL = 'url';

    public function __construct(SuggestedCategoryImage $suggestedCategoryImageType)
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'Category data displayed in the homepage. '.\PHP_EOL.
                'This entity intentionally does not reference the main "Category" entity, because considering its '.
                'planned usage (exclusive to the homepage), there is no obvious interest in doing so. '.\PHP_EOL.
                'The choice has been made to also not extend the "Category" entity, as the homepage data presented '.
                'here should be exclusive to the homepage context.'.\PHP_EOL.
                'Because it should remain exclusive to the homepage context, this is also not a shareable or '.
                'extensible entity.',
            'fields' => [
                self::FIELD_ID => [
                    'description' => 'Identifier of the associated category',
                    'type' => new NonNull(Type::id()),
                ],
                self::FIELD_FAMILY_ID => [
                    'description' => 'Identifier of the top-most category (family) to display',
                    'type' => new NonNull(Type::id()),
                ],
                self::FIELD_NAME => [
                    'description' => 'Name to use, not necessary the same as the associated "Category"',
                    'type' => new NonNull(Type::string()),
                ],
                self::FIELD_IMAGE => [
                    'description' => 'All image-related data',
                    'type' => new NonNull($suggestedCategoryImageType),
                ],
                self::FIELD_URL => [
                    'description' => 'Relative URL to the category page, not necessary the same as the slug of the associated "Category"',
                    'type' => new NonNull(Type::string()),
                ],
            ],
        ]);
    }
}
