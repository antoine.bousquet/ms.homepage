<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use Apollo\Federation\Types\EntityObjectType;
use App\Application\GraphQL\CappedListResolverTrait;
use App\Application\GraphQL\Exception\MissingMandatoryFieldException;
use App\Application\GraphQL\NonBlockingErrorsCollection;
use App\Application\GraphQL\Type\Enum\Channel as ChannelType;
use App\Application\GraphQL\Type\Enum\Market as MarketType;
use App\Application\GraphQL\Type\Enum\Platform as PlatformType;
use App\Domain\Entity\Homepage\Advice;
use App\Domain\Entity\Homepage\PromotedBrand;
use App\Domain\Entity\Homepage\PromotedProduct;
use App\Domain\Enum\Channel;
use App\Domain\Enum\Market;
use App\Domain\Enum\Platform;
use App\Domain\Service\RawJsonBlockService;
use App\Infrastructure\Repository\AdviceRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ResolveInfo;
use Webmozart\Assert\Assert;

final class Homepage extends EntityObjectType
{
    use CappedListResolverTrait;

    public const TYPE_NAME = 'Homepage';

    public const FIELD_PLATFORM = 'platform';
    public const FIELD_MARKET = 'market';
    public const FIELD_CHANNEL = 'channel';
    public const FIELD_BRANDS = 'brands';
    public const FIELD_ADVICE = 'advice';
    public const FIELD_SUGGESTED_CATEGORIES = 'suggestedCategories';
    public const FIELD_PROMOTED_PRODUCTS = 'promotedProducts';

    public const KEY_FIELDS = [
        self::FIELD_PLATFORM,
        self::FIELD_MARKET,
        self::FIELD_CHANNEL,
    ];

    public function __construct(
        NonBlockingErrorsCollection $errorsCollection,
        private readonly PromotedBrandRepositoryInterface $promotedBrandRepository,
        private readonly AdviceRepositoryInterface $adviceRepository,
        private readonly PromotedProductRepositoryInterface $promotedProductRepository,
        private readonly RawJsonBlockService $rawJsonBlockService,
        PlatformType $platformType,
        MarketType $marketType,
        ChannelType $channelType,
        Brand $brandType,
        Tipsheet $tipsheetType,
        SuggestedCategory $suggestedCategoryType,
        ProductCollection $productCollectionType,
    ) {
        $this->errorCollection = $errorsCollection;
        parent::__construct([
            'name' => self::TYPE_NAME,
            'keyFields' => [implode(' ', self::KEY_FIELDS)],
            'description' => 'Content required to display the home page',
            'fields' => [
                self::FIELD_PLATFORM => [
                    'description' => 'Associated platform',
                    'type' => new NonNull($platformType),
                    'resolve' => self::resolvePlatform(...),
                ],
                self::FIELD_MARKET => [
                    'description' => 'Associated market',
                    'type' => new NonNull($marketType),
                    'resolve' => self::resolveMarket(...),
                ],
                self::FIELD_CHANNEL => [
                    'description' => 'Associated channel',
                    'type' => new NonNull($channelType),
                    'resolve' => self::resolveChannel(...),
                ],
                self::FIELD_BRANDS => [
                    'description' => 'All brands attached to the provided platform/channel/market, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                    'type' => new NonNull(new ListOfType(new NonNull($brandType))),
                    'resolve' => self::resolveBrands(...),
                ],
                self::FIELD_ADVICE => [
                    'description' => 'All advice attached to the provided platform/channel/market, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                    'type' => new NonNull(new ListOfType(new NonNull($tipsheetType))),
                    'resolve' => self::resolveAdvice(...),
                ],
                self::FIELD_SUGGESTED_CATEGORIES => [
                    'description' => 'All suggested categories attached to the provided platform/channel/market, sorted by their configured position.',
                    'type' => new NonNull(new ListOfType(new NonNull($suggestedCategoryType))),
                    'resolve' => self::resolveSuggestedCategories(...),
                ],
                self::FIELD_PROMOTED_PRODUCTS => [
                    'description' => 'All promoted products attached to the provided platform/channel/market, fetched from their model identifiers, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                    'type' => new NonNull($productCollectionType),
                    'resolve' => self::resolvePromotedProducts(...),
                ],
            ],
            '__resolveReference' => self::resolveHomepageReference(...),
        ]);
    }

    /**
     * @param array<string, mixed> $reference
     */
    private static function getTypeNameFromReference(array $reference, ?string $expected = null): string
    {
        /**
         * @var string $typeName
         */
        $typeName = $reference['__typename'] ?? null;
        if (null === $typeName) {
            throw new \InvalidArgumentException('The provided reference does not have the mandatory "__typename" property.');
        }
        if (null !== $expected && $expected !== $typeName) {
            throw new \InvalidArgumentException(sprintf('The provided reference type is invalid: got "%s", was expecting "%s".', $typeName, $expected));
        }

        return $typeName;
    }

    /**
     * @param array<string, mixed> $reference
     */
    private static function getEnumFromReference(array $reference, string $fieldName): mixed
    {
        $typeName = self::getTypeNameFromReference($reference);
        $string = $reference[$fieldName] ?? null;
        if (null === $string) {
            throw new MissingMandatoryFieldException($typeName, $fieldName);
        }

        return $string;
    }

    /**
     * @param array{__typename: string, platform: string, market: string, channel: string} $reference
     *
     * @return array{__typename: string, platform: Platform, market: Market, channel: Channel}
     */
    private static function resolveHomepageReference(array $reference): array
    {
        $typeName = $reference['__typename'] ?? null;
        if (null === $typeName) {
            throw new \InvalidArgumentException('The provided reference does not have the mandatory "__typename" property.');
        }
        if (self::TYPE_NAME !== $typeName) {
            throw new \InvalidArgumentException(sprintf('The provided reference type is invalid: got "%s", was expecting "%s".', $typeName, self::TYPE_NAME));
        }

        // Get platform
        $platformString = self::getEnumFromReference($reference, self::FIELD_PLATFORM);
        Assert::inArray($platformString, Platform::casesString());
        $platform = Platform::from($platformString);

        // Get market
        $marketString = self::getEnumFromReference($reference, self::FIELD_MARKET);
        Assert::inArray($marketString, Market::casesString());
        $market = Market::from($marketString);

        // Get channel
        $channelString = self::getEnumFromReference($reference, self::FIELD_CHANNEL);
        Assert::inArray($channelString, Channel::casesString());
        $channel = Channel::from($channelString);

        /*
         * Homepage type
         *
         * "__typename" HAS to be provided in all returns of functions used with "__resolveReference". If not, the
         * apollo federation function "_entities" will fail for this specific type.
         */
        return [
            '__typename' => self::TYPE_NAME,
            self::FIELD_PLATFORM => $platform,
            self::FIELD_MARKET => $market,
            self::FIELD_CHANNEL => $channel,
        ];
    }

    /**
     * @param array{market: Market} $homepage
     */
    private static function resolveMarket(array $homepage): Market
    {
        return $homepage[self::FIELD_MARKET];
    }

    /**
     * @param array{platform: Platform} $homepage
     */
    private static function resolvePlatform(array $homepage): Platform
    {
        return $homepage[self::FIELD_PLATFORM];
    }

    /**
     * @param array{channel: Channel} $homepage
     */
    private static function resolveChannel(array $homepage): Channel
    {
        return $homepage[self::FIELD_CHANNEL];
    }

    /**
     * Checks whether the channel is "WEB" and the market "B2C".
     *
     * When channel is not "WEB" and market is not "B2C", we want to skip some results as the microservice does not
     * handle these properly for now.
     *
     * @param array{market: Market, channel: Channel} $homepage
     */
    private static function isWebChannelAndB2CMarket(array $homepage): bool
    {
        return Channel::WEB === $homepage[self::FIELD_CHANNEL] && Market::B2C === $homepage[self::FIELD_MARKET];
    }

    /**
     * @param array{platform: Platform, market: Market, channel: Channel} $homepage
     * @param array<string, mixed>                                        $args
     *
     * @return array<array{brandId: positive-int}>
     */
    private function resolveBrands(array $homepage, array $args, mixed $context, ResolveInfo $info): array
    {
        if (!self::isWebChannelAndB2CMarket($homepage)) {
            return [];
        }

        $platform = $homepage[self::FIELD_PLATFORM];
        $market = $homepage[self::FIELD_MARKET];

        $promotedBrandsResult = $this->promotedBrandRepository->list(
            ['platformId' => $platform->value],
            ['position' => ListRepositoryInterface::SORT_ORDER_ASC],
        );
        $this->handleListMaximum($promotedBrandsResult, ListRepositoryInterface::MAXIMUM_LIMIT, $info);
        /**
         * @var array<PromotedBrand>
         */
        $promotedBrands = $promotedBrandsResult->getItems();

        /**
         * @return array{brandId: positive-int}
         */
        $toGraphQL = static function (PromotedBrand $promotedBrand) use ($platform, $market): array {
            // Brand type
            return [
                Brand::FIELD_ID => $promotedBrand->getBrandId(),
                Brand::FIELD_PLATFORM => $platform,
                Brand::FIELD_MARKET => $market,
            ];
        };

        return array_map($toGraphQL, $promotedBrands);
    }

    /**
     * @param array{platform: Platform, market: Market, channel: Channel} $homepage
     * @param array<string, mixed>                                        $args
     *
     * @return array<array{id: positive-int}>
     */
    private function resolveAdvice(array $homepage, array $args, mixed $context, ResolveInfo $info): array
    {
        if (!self::isWebChannelAndB2CMarket($homepage)) {
            return [];
        }

        $adviceResult = $this->adviceRepository->list(
            ['platformId' => $homepage[self::FIELD_PLATFORM]->value],
            ['position' => ListRepositoryInterface::SORT_ORDER_ASC]
        );
        $this->handleListMaximum($adviceResult, ListRepositoryInterface::MAXIMUM_LIMIT, $info);
        /**
         * @var array<Advice>
         */
        $advice = $adviceResult->getItems();

        /**
         * @return array{id: positive-int}
         */
        $toGraphQL = static function (Advice $advice): array {
            // Tipsheet type
            return [
                Tipsheet::FIELD_ID => $advice->getTipsheetId(),
            ];
        };

        return array_map($toGraphQL, $advice);
    }

    /**
     * @param array{platform: Platform, market: Market, channel: Channel} $homepage
     *
     * @return array<string, mixed> SuggestedCategory representation
     */
    private function resolveSuggestedCategories(array $homepage): array
    {
        if (!self::isWebChannelAndB2CMarket($homepage)) {
            return [];
        }

        /**
         * @return array<string, mixed> SuggestedCategory representation
         */
        $toGraphQL = static function (array $json): array {
            // SuggestedCategory type
            return [
                'id' => $json['id'],
                'familyId' => $json['family_id'],
                'name' => $json['name'],
                'image' => [
                    'smallUrl' => $json['image']['small_url'],
                ],
                'url' => $json['url'],
            ];
        };

        $json = $this->rawJsonBlockService->renderSuggestedCategoriesBlock($homepage[self::FIELD_PLATFORM]);

        return array_map($toGraphQL, $json);
    }

    /**
     * @param array{platform: Platform, market: Market, channel: Channel} $homepage
     * @param array<string, mixed>                                        $args
     *
     * @return array<string, mixed> ProductCollection reference
     */
    private function resolvePromotedProducts(array $homepage, array $args, mixed $context, ResolveInfo $info): array
    {
        if (Channel::WEB !== $homepage[self::FIELD_CHANNEL]) {
            return [];
        }

        $promotedProductsResult = $this->promotedProductRepository->list(
            ['platformId' => $homepage[self::FIELD_PLATFORM]->value],
            ['position' => ListRepositoryInterface::SORT_ORDER_ASC],
        );
        $this->handleListMaximum($promotedProductsResult, ListRepositoryInterface::MAXIMUM_LIMIT, $info);
        /**
         * @var array<PromotedProduct>
         */
        $promotedProducts = $promotedProductsResult->getItems();

        $modelIds = array_map(static function (PromotedProduct $promotedProduct): int {
            return $promotedProduct->getModelId();
        }, $promotedProducts);

        // ProductCollection type
        // The ProductCollection will return the products in the order of the provided model identifiers
        return [
            'platform' => $homepage[self::FIELD_PLATFORM],
            'isPro' => (Market::B2B === $homepage[self::FIELD_MARKET]),
            'modelIds' => $modelIds,
            // Since we expect to end up with the same number of Products than PromotedProducts, we use the same maximum value
            'limit' => ListRepositoryInterface::MAXIMUM_LIMIT,
        ];
    }
}
