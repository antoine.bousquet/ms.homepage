<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use Apollo\Federation\Types\EntityRefObjectType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\Type;

final class Tipsheet extends EntityRefObjectType
{
    public const TYPE_NAME = 'Tipsheet';
    public const FIELD_ID = 'id';

    public function __construct()
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'keyFields' => [self::FIELD_ID],
            'description' => 'Tipsheet data',
            'fields' => [
                self::FIELD_ID => [
                    'description' => 'Tipsheet identifier',
                    'type' => new NonNull(Type::int()),
                    'isExternal' => true,
                ],
            ],
        ]);
    }
}
