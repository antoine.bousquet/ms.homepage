<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Entity;

use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

final class SuggestedCategoryImage extends ObjectType
{
    public const TYPE_NAME = 'SuggestedCategoryImage';

    public const FIELD_SMALL_URL = 'smallUrl';

    public function __construct()
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'Image data for the suggested categories. '.\PHP_EOL.
                'This is not a shareable or extensible entity as its planned usage limits it to the homepage.',
            'fields' => [
                self::FIELD_SMALL_URL => [
                    'description' => 'URL path to the regular image',
                    'type' => new NonNull(Type::string()),
                ],
            ],
        ]);
    }
}
