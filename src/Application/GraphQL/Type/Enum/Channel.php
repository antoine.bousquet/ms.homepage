<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Enum;

use App\Domain\Enum\Channel as ChannelEnum;
use GraphQL\Type\Definition\EnumType;

final class Channel extends EnumType
{
    public const TYPE_NAME = 'Channel';

    public function __construct()
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'One of the available channels',
            'values' => [
                ChannelEnum::WEB->value => [
                    'value' => ChannelEnum::WEB,
                    'description' => 'Web context',
                ],
                ChannelEnum::MOBILE->value => [
                    'value' => ChannelEnum::MOBILE,
                    'description' => 'Mobile context',
                ],
            ],
        ]);
    }
}
