<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Enum;

use App\Domain\Enum\Platform as PlatformEnum;
use GraphQL\Type\Definition\EnumType;

final class Platform extends EnumType
{
    public const TYPE_NAME = 'Platform';

    public function __construct()
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'One of the available platforms',
            'values' => [
                PlatformEnum::FRANCE->value => [
                    'value' => PlatformEnum::FRANCE,
                    'description' => 'France',
                ],
                PlatformEnum::SPAIN->value => [
                    'value' => PlatformEnum::SPAIN,
                    'description' => 'Spain',
                ],
                PlatformEnum::ITALY->value => [
                    'value' => PlatformEnum::ITALY,
                    'description' => 'Italy',
                ],
                PlatformEnum::GREAT_BRITAIN->value => [
                    'value' => PlatformEnum::GREAT_BRITAIN,
                    'description' => 'Great Britain',
                ],
                PlatformEnum::DEUTSCHLAND->value => [
                    'value' => PlatformEnum::DEUTSCHLAND,
                    'description' => 'Germany',
                ],
            ],
        ]);
    }
}
