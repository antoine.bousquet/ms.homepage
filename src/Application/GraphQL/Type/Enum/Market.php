<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Enum;

use App\Domain\Enum\Market as MarketEnum;
use GraphQL\Type\Definition\EnumType;

final class Market extends EnumType
{
    public const TYPE_NAME = 'Market';

    public function __construct()
    {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'One of the available markets',
            'values' => [
                MarketEnum::B2C->value => [
                    'value' => MarketEnum::B2C,
                    'description' => 'Business to Customer',
                ],
                MarketEnum::B2B->value => [
                    'value' => MarketEnum::B2B,
                    'description' => 'Business to Business',
                ],
            ],
        ]);
    }
}
