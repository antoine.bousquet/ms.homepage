<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Type\Scalar;

use GraphQL\Error\Error;
use GraphQL\Type\Definition\IntType;

final class BigInteger extends IntType
{
    public const TYPE_NAME = 'BigInteger';

    public const MAX_INT = 10E100;
    public const MIN_INT = -10E100;

    public $name = self::TYPE_NAME;

    public $description = 'A scalar type representing big integers.';

    /**
     * @throws Error
     */
    public function serialize(mixed $value): string // @phpstan-ignore-line
    {
        return (string) parent::serialize($value);
    }
}
