<?php

declare(strict_types=1);

namespace App\Application\GraphQL;

use App\Application\GraphQL\Type\Entity\Homepage;
use App\Application\GraphQL\Type\Enum\Channel as ChannelType;
use App\Application\GraphQL\Type\Enum\Market as MarketType;
use App\Application\GraphQL\Type\Enum\Platform as PlatformType;
use App\Domain\Enum\Channel;
use App\Domain\Enum\Market;
use App\Domain\Enum\Platform;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ObjectType;

final class Query extends ObjectType
{
    public const TYPE_NAME = 'Query';

    public const FIELD_HOMEPAGE = 'homepage';

    public function __construct(
        Homepage $homepageType,
        PlatformType $platformType,
        MarketType $marketType,
        ChannelType $channelType,
    ) {
        parent::__construct([
            'name' => self::TYPE_NAME,
            'description' => 'The global query object',
            'fields' => [
                self::FIELD_HOMEPAGE => [
                    'type' => $homepageType,
                    'description' => 'Homepage data',
                    'args' => [
                        Homepage::FIELD_PLATFORM => [
                            'description' => 'Associated platform',
                            'type' => new NonNull($platformType),
                        ],
                        Homepage::FIELD_MARKET => [
                            'description' => 'Associated market',
                            'type' => new NonNull($marketType),
                        ],
                        Homepage::FIELD_CHANNEL => [
                            'description' => 'Associated channel',
                            'type' => new NonNull($channelType),
                        ],
                    ],
                    'resolve' => self::resolveHomepage(...),
                ],
            ],
        ]);
    }

    /**
     * @param array{platform: Platform, market: Market, channel: Channel} $args
     *
     * @return array{platform: Platform, market: Market, channel: Channel}
     */
    private static function resolveHomepage(mixed $objectValue, array $args): array
    {
        $platform = $args[Homepage::FIELD_PLATFORM];
        $market = $args[Homepage::FIELD_MARKET];
        $channel = $args[Homepage::FIELD_CHANNEL];

        return [
            Homepage::FIELD_PLATFORM => $platform,
            Homepage::FIELD_MARKET => $market,
            Homepage::FIELD_CHANNEL => $channel,
        ];
    }
}
