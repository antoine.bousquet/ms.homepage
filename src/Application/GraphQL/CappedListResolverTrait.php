<?php

declare(strict_types=1);

namespace App\Application\GraphQL;

use App\Domain\Entity\ListResult;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;

trait CappedListResolverTrait
{
    private NonBlockingErrorsCollection $errorCollection;

    private function handleListMaximum(ListResult $listResult, int $maximumLimit, ResolveInfo $info): void
    {
        if ($listResult->getCount() > $maximumLimit) {
            $error = new Error(sprintf(
                'The result list was truncated because the maximum allowed number of elements to display was reached (%d total items, %d displayed)',
                $listResult->getCount(),
                $maximumLimit,
            ));
            $this->errorCollection->addError($error, $info);
        }
    }
}
