<?php

declare(strict_types=1);

namespace App\Application\GraphQL;

use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;

final class NonBlockingErrorsCollection
{
    /**
     * @var array<Error>
     */
    private array $errors = [];

    public function addError(Error $error, ResolveInfo $info): void
    {
        $locatedError = Error::createLocatedError($error, $info->fieldNodes, $info->path);
        $this->errors[] = $locatedError;
    }

    /**
     * @return array<Error>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
