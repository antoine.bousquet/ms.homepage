<?php

declare(strict_types=1);

namespace App\Application\GraphQL;

use App\Application\GraphQL\Type\Entity\Brand;
use App\Application\GraphQL\Type\Entity\Homepage;
use App\Application\GraphQL\Type\Entity\Tipsheet;
use App\Application\GraphQL\Type\Enum\Platform as PlatformType;
use App\Domain\Entity\Homepage\Advice;
use App\Domain\Entity\Homepage\PromotedBrand;
use App\Domain\Enum\Market;
use App\Domain\Enum\Platform;
use App\Infrastructure\Repository\AdviceRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;

final class LegacyQueryDecorator extends ObjectType
{
    use CappedListResolverTrait;

    public const LEGACY_FIELD_B2C_BRANDS = 'homepageB2cBrands';
    public const LEGACY_FIELD_B2C_BRANDS_V1 = 'homepageB2cBrandsV1';
    public const LEGACY_FIELD_B2C_TIPSHEETS = 'homepageB2cTipsheets';
    public const LEGACY_FIELD_B2C_TIPSHEETS_V1 = 'homepageB2cTipsheetsV1';

    public const ARGUMENT_PLATFORM = 'platform';

    public function __construct(
        NonBlockingErrorsCollection $errorsCollection,
        private readonly PromotedBrandRepositoryInterface $promotedBrandRepository,
        private readonly AdviceRepositoryInterface $adviceRepository,
        Query $queryType,
        PlatformType $platformType,
        Brand $brandType,
        Tipsheet $tipsheetType,
    ) {
        $this->errorCollection = $errorsCollection;

        /**
         * @var array{fields: array<string, mixed>}
         */
        $config = $queryType->config;
        $config['fields'] = array_replace($config['fields'], [
            self::LEGACY_FIELD_B2C_BRANDS => [
                'type' => new NonNull(new ListOfType(new NonNull($brandType))),
                'description' => 'Legacy - will fetch homepage brands for the provided platform, on B2C market and WEB channel, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                'args' => [
                    self::ARGUMENT_PLATFORM => [
                        'description' => 'Associated platform',
                        'type' => new NonNull($platformType),
                    ],
                ],
                'resolve' => self::resolveLegacyB2cBrands(...),
                'deprecationReason' => 'Add nullability. Use query "homepage" instead. Removal on 2023-01-10',
            ],
            self::LEGACY_FIELD_B2C_BRANDS_V1 => [
                'type' => new ListOfType(new NonNull($brandType)),
                'description' => 'Legacy - will fetch homepage brands for the provided platform, on B2C market and WEB channel, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                'args' => [
                    self::ARGUMENT_PLATFORM => [
                        'description' => 'Associated platform',
                        'type' => new NonNull($platformType),
                    ],
                ],
                'resolve' => self::resolveLegacyB2cBrandsV1(...),
                'deprecationReason' => 'Full rework. Use query "homepage" instead. Removal on 2023-10-01',
            ],
            self::LEGACY_FIELD_B2C_TIPSHEETS => [
                'type' => new NonNull(new ListOfType(new NonNull($tipsheetType))),
                'description' => 'Legacy - will fetch homepage tipsheets for the provided platform, on B2C market and WEB channel, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                'args' => [
                    self::ARGUMENT_PLATFORM => [
                        'description' => 'Associated platform',
                        'type' => new NonNull($platformType),
                    ],
                ],
                'resolve' => self::resolveLegacyB2cTipsheets(...),
                'deprecationReason' => 'Add nullability. Use query "homepage" instead. Removal on 2023-01-10',
            ],
            self::LEGACY_FIELD_B2C_TIPSHEETS_V1 => [
                'type' => new ListOfType(new NonNull($tipsheetType)),
                'description' => 'Legacy - will fetch homepage tipsheets for the provided platform, on B2C market and WEB channel, sorted by their configured position. Capped at 1000 elements, an error is emitted if the maximum is reached.',
                'args' => [
                    self::ARGUMENT_PLATFORM => [
                        'description' => 'Associated platform',
                        'type' => new NonNull($platformType),
                    ],
                ],
                'resolve' => self::resolveLegacyB2cTipsheetsV1(...),
                'deprecationReason' => 'Full rework. Use query "homepage" instead. Removal on 2023-10-01',
            ],
        ]);

        parent::__construct($config);
    }

    /**
     * @param array{platform: Platform} $args
     *
     * @return array<array<string, mixed>> Brand reference list
     */
    private function resolveLegacyB2cBrands(mixed $objectValue, array $args, mixed $context, ResolveInfo $info): array
    {
        $platform = $args[Homepage::FIELD_PLATFORM];
        $market = Market::B2C;

        $promotedBrandsResult = $this->promotedBrandRepository->list(
            ['platformId' => $platform->value],
            ['position' => ListRepositoryInterface::SORT_ORDER_ASC],
        );
        $this->handleListMaximum($promotedBrandsResult, ListRepositoryInterface::MAXIMUM_LIMIT, $info);
        /**
         * @var array<PromotedBrand>
         */
        $promotedBrands = $promotedBrandsResult->getItems();

        /**
         * @return array<string, mixed> Brand reference
         */
        $toGraphQL = static function (PromotedBrand $brand) use ($platform, $market): array {
            // Brand type
            return [
                'brandId' => $brand->getBrandId(),
                'platform' => $platform,
                'market' => $market,
            ];
        };

        return array_map($toGraphQL, $promotedBrands);
    }

    /**
     * @param array{platform: Platform} $args
     *
     * @return array<array<string, mixed>> Brand reference list
     */
    private function resolveLegacyB2cBrandsV1(mixed $objectValue, array $args, mixed $context, ResolveInfo $info): array
    {
        return $this->resolveLegacyB2cBrands($objectValue, $args, $context, $info);
    }

    /**
     * @param array{platform: Platform} $args
     *
     * @return array<array{id: positive-int}> Tipsheet reference list
     */
    private function resolveLegacyB2cTipsheets(mixed $objectValue, array $args, mixed $context, ResolveInfo $info): array
    {
        $platform = $args[Homepage::FIELD_PLATFORM];
        $adviceResult = $this->adviceRepository->list(
            ['platformId' => $platform->value],
            ['position' => ListRepositoryInterface::SORT_ORDER_ASC],
        );
        $this->handleListMaximum($adviceResult, ListRepositoryInterface::MAXIMUM_LIMIT, $info);
        /**
         * @var array<Advice>
         */
        $advice = $adviceResult->getItems();

        /**
         * @return array{id: positive-int} Tipsheet reference
         */
        $toGraphQL = static function (Advice $advice): array {
            // Tipsheet type
            return [
                'id' => $advice->getTipsheetId(),
            ];
        };

        return array_map($toGraphQL, $advice);
    }

    /**
     * @param array{platform: Platform} $args
     *
     * @return array<array{id: positive-int}> Tipsheet reference list
     */
    private function resolveLegacyB2cTipsheetsV1(mixed $objectValue, array $args, mixed $context, ResolveInfo $info): array
    {
        return $this->resolveLegacyB2cTipsheets($objectValue, $args, $context, $info);
    }
}
