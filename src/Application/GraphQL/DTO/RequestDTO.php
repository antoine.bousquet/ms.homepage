<?php

declare(strict_types=1);

namespace App\Application\GraphQL\DTO;

final class RequestDTO
{
    /**
     * @param array<string, mixed> $variables
     */
    public function __construct(
        private readonly string $query,
        private readonly ?string $operationName,
        private readonly array $variables = [],
    ) {
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getOperationName(): ?string
    {
        return $this->operationName;
    }

    /**
     * @return array<string, mixed>
     */
    public function getVariables(): array
    {
        return $this->variables;
    }
}
