<?php

declare(strict_types=1);

namespace App\Application\GraphQL\Exception;

final class MissingMandatoryFieldException extends \InvalidArgumentException
{
    public function __construct(string $typeName, string $fieldName)
    {
        $message = sprintf('The field "%s" is mandatory in the "%s" reference', $fieldName, $typeName);
        parent::__construct($message);
    }
}
