<?php

declare(strict_types=1);

namespace App\Application\Action\V1;

use App\Application\GraphQL\DTO\RequestDTO;
use App\Application\GraphQL\NonBlockingErrorsCollection;
use App\Application\GraphQL\Schema;
use GraphQL\GraphQL;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GraphQLAction
{
    public function __construct(
        private readonly Schema $schema,
        private readonly RequestToDTOConverter $requestToDTOConverter,
        private readonly NonBlockingErrorsCollection $errorCollection,
        protected readonly int $cacheResponseHeaderTimeToLive,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $useCache = true;

        /**
         * @var RequestDTO
         */
        $dto = $this->requestToDTOConverter->convert($request, RequestDTO::class);
        $result = GraphQL::executeQuery(
            $this->schema,
            $dto->getQuery(),
            null,
            null,
            $dto->getVariables(),
            $dto->getOperationName(),
        );
        $result->errors = array_merge($result->errors, $this->errorCollection->getErrors());
        foreach ($result->errors as $error) {
            $useCache = false;
            $this->logger->error($error->getMessage(), [
                'code' => $error->getCode(),
                'path' => $error->getPath(),
                'file' => $error->getFile(),
                'line' => $error->getLine(),
                'positions' => $error->getPositions(),
                'locations' => $error->getLocations(),
            ]);
        }

        $response = new JsonResponse($result->toArray(), Response::HTTP_OK);
        if ($useCache) {
            $response->setPublic();
            $response->setMaxAge($this->cacheResponseHeaderTimeToLive);
            $response->setSharedMaxAge($this->cacheResponseHeaderTimeToLive);
        }

        return $response;
    }
}
