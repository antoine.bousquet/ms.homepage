<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD;

use App\Application\Action\V1\AbstractAction;
use App\Application\Action\V1\CRUD\Traits\GetResourceNameTrait;
use App\Domain\Converter\DtoToEntityConverter;
use App\Domain\DTO\DTOInterface;
use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\Homepage\AdministrationLog;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\CRUD\CreateRepositoryInterface;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractCreateAction extends AbstractAction
{
    use GetResourceNameTrait;

    /**
     * @param class-string<DTOInterface> $dtoClassName
     */
    public function __construct(
        protected readonly string $dtoClassName,
        private readonly CreateRepositoryInterface $repository,
        private readonly AdministrationLogRepositoryInterface $administrationLogRepository,
        private readonly RequestToDTOConverter $requestToDTOConverter,
        private readonly DtoToEntityConverter $dtoToEntityConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
        $this->resourceName = null;
    }

    public function __invoke(Request $request): JsonResponse
    {
        /**
         * @var DTOInterface $dto
         */
        $dto = $this->requestToDTOConverter->convert($request, $this->dtoClassName);
        $entity = $this->repository->createNew();
        $entity = $this->dtoToEntityConverter->convert($entity, $dto);
        $this->repository->save($entity);

        // If this is an administrable content, we want to save a log of any modification
        if ($entity instanceof AdministrableResourceEntityInterface) {
            $log = $this->administrationLogRepository->createFromAdministrableResourceEntity(
                $entity,
                AdministrationLog::ACTION_CREATE,
                $this->getResourceName(\get_class($entity))
            );
            $this->administrationLogRepository->save($log);
        }

        $this->repository->flush();

        $this->generateMetadata($entity);

        return $this->getJsonResponseForData(
            $entity,
            Response::HTTP_CREATED,
            null,
            false,
            [],
            false,
        );
    }

    protected function generateMetadata(EntityInterface $entity): void
    {
    }
}
