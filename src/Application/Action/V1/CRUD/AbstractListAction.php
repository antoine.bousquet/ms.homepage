<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD;

use App\Application\Action\V1\AbstractAction;
use App\Domain\DTO\ListOutputDTO;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use App\Infrastructure\Repository\CRUD\ListRepositoryInterface;
use App\Infrastructure\Repository\Exception\InvalidFilterException;
use App\Infrastructure\Repository\Exception\InvalidFilterValueException;
use App\Infrastructure\Repository\Exception\InvalidLimitException;
use App\Infrastructure\Repository\Exception\InvalidSortException;
use Manomano\ApiSpecs\Pagination\Pagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractListAction extends AbstractAction
{
    public function __construct(
        private readonly ListRepositoryInterface $repository,
        private readonly CamelCaseToSnakeCaseNameConverter $nameConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
    }

    public function __invoke(Request $request): JsonResponse
    {
        try {
            $sort = $this->extractSortsFromRequest($request);
            $listResult = $this->repository->list(
                $this->extractFiltersFromRequest($request),
                null === $sort ? [] : [$sort['field'] => $sort['order']],
                $limit = $this->extractLimitFromRequest($request),
                $page = $this->extractPageFromRequest($request),
                GetRepositoryInterface::CONTEXT_WRITE
            );

            /**
             * @var array<mixed> $items
             */
            $items = $listResult->getItems();

            return $this->getListOutputDTOJsonResponse(
                $items,
                $page,
                $limit,
                $listResult->getCount(),
                !$request->isNoCache()
            );
        } catch (InvalidFilterException $exception) {
            $filter = $this->nameConverter->normalize($exception->getFilter());
            $availableFilters = [];
            foreach ($exception->getAvailableFilters() as $availableFilter) {
                $availableFilters[] = $this->nameConverter->normalize($availableFilter);
            }

            throw new BadRequestHttpException(sprintf('Filter "%s" is invalid. Valid filters: %s', $filter, implode(', ', $availableFilters)));
        } catch (InvalidFilterValueException $exception) {
            $filter = $this->nameConverter->normalize($exception->getFilter());
            $message = str_replace('{{ filter }}', $filter, $exception->getMessageTemplate());

            throw new BadRequestHttpException($message);
        } catch (InvalidSortException $exception) {
            $sort = $this->nameConverter->normalize($exception->getSortField());
            $availableSorts = [];
            foreach ($exception->getAvailableSorts() as $availableSort) {
                $availableSorts[] = $this->nameConverter->normalize($availableSort);
            }

            throw new BadRequestHttpException(sprintf('Sort field "%s" is invalid. Valid sort fields: %s', $sort, implode(', ', $availableSorts)));
        } catch (InvalidLimitException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    /**
     * @param array<mixed> $data
     */
    protected function getListOutputDTOJsonResponse(array $data, int $page, int $limit, int $count, bool $useCache = true): JsonResponse
    {
        $dto = new ListOutputDTO($data, new Pagination(
            $page,
            $limit,
            $count,
        ));

        return $this->getJsonResponseForData(
            $dto,
            Response::HTTP_OK,
            null,
            false,
            ['Content-Range' => $count],
            $useCache,
        );
    }

    /**
     * @return array<string, mixed>|null
     */
    private function extractJsonArrayFromQueryParameter(string $parameterName, Request $request): ?array
    {
        /**
         * @var string|int|float|bool|null $parameter
         */
        $parameter = $request->query->get($parameterName);
        if (null === $parameter) {
            return null;
        }
        if (!\is_string($parameter)) {
            throw new BadRequestHttpException(sprintf('The query parameter "%s" should be a JSON string, "%s" given.', $parameterName, \gettype($parameter)));
        }
        $parameter = json_decode($parameter, true);
        if (\JSON_ERROR_NONE !== json_last_error()) {
            throw new BadRequestHttpException(sprintf('The query parameter "%s" should be JSON, but the following error happened when deserializing it: %s', $parameterName, json_last_error_msg()));
        }
        if (!\is_array($parameter)) {
            throw new BadRequestHttpException(sprintf('The query parameter "%s" should be a JSON map, "%s" provided.', $parameterName, \gettype($parameter)));
        }

        return $parameter;
    }

    /**
     * @return array<string, mixed>
     */
    private function extractFiltersFromRequest(Request $request): array
    {
        $filters = $this->extractJsonArrayFromQueryParameter('filter', $request);
        if (null === $filters) {
            return [];
        }

        $convertedFilters = [];
        foreach ($filters as $filter => $value) {
            $convertedFilters[$this->nameConverter->denormalize($filter)] = $value;
        }

        return $convertedFilters;
    }

    /**
     * @return array<string, string>|null
     */
    private function extractSortsFromRequest(Request $request): ?array
    {
        $sort = $this->extractJsonArrayFromQueryParameter('sort', $request);
        if (null === $sort) {
            return null;
        }

        if (!isset($sort['field']) || !\is_string($sort['field'])) {
            throw new BadRequestHttpException('The sorting parameter must include a "field" key that maps to a string.');
        }
        if (!isset($sort['order']) || !\in_array($sort['order'], ListRepositoryInterface::SORT_ORDERS, true)) {
            throw new BadRequestHttpException('The sorting parameter must include a "order" key that must be either "ASC" or "DESC"');
        }

        return [
            'field' => $this->nameConverter->denormalize($sort['field']),
            'order' => $sort['order'],
        ];
    }

    /**
     * @return positive-int|null
     */
    private function extractStrictlyPositiveIntegerFromRequest(string $parameterName, Request $request): ?int
    {
        /**
         * @var string|int|float|bool|null $parameter
         */
        $parameter = $request->query->get($parameterName);
        if (null === $parameter) {
            return null;
        }
        if (!\is_int($parameter) && !(\is_string($parameter) && (!\in_array(preg_match('/^\d+$/', $parameter), [false, 0], true) && '0' !== $parameter))) {
            throw new BadRequestHttpException(sprintf('The parameter "%s" must be a strictly positive integer.', $parameterName));
        }

        // @phpstan-ignore-next-line
        return (int) $parameter;
    }

    /**
     * @return positive-int
     */
    private function extractLimitFromRequest(Request $request): int
    {
        return $this->extractStrictlyPositiveIntegerFromRequest('limit', $request)
            ?? ListRepositoryInterface::MAXIMUM_LIMIT;
    }

    /**
     * @return positive-int
     */
    private function extractPageFromRequest(Request $request): int
    {
        return $this->extractStrictlyPositiveIntegerFromRequest('page', $request)
            ?? ListRepositoryInterface::DEFAULT_PAGE;
    }
}
