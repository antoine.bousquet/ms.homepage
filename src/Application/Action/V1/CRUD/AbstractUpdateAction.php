<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD;

use App\Application\Action\V1\AbstractAction;
use App\Application\Action\V1\CRUD\Traits\ExtractEntityFromRequestTrait;
use App\Domain\Converter\DtoToEntityConverter;
use App\Domain\DTO\DTOInterface;
use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\Homepage\AdministrationLog;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\CRUD\UpdateRepositoryInterface;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractUpdateAction extends AbstractAction
{
    use ExtractEntityFromRequestTrait;

    /**
     * @param class-string<DTOInterface> $dtoClassName
     */
    public function __construct(
        protected readonly string $dtoClassName,
        private readonly UpdateRepositoryInterface $repository,
        private readonly AdministrationLogRepositoryInterface $administrationLogRepository,
        private readonly RequestToDTOConverter $requestToDTOConverter,
        private readonly DtoToEntityConverter $dtoToEntityConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
        $this->resourceName = null;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $entity = $this->extractEntityFromRequest($request, $this->repository);
        $dto = $this->requestToDTOConverter->convert($request, $this->dtoClassName);
        /** @var DTOInterface $dto */
        $entity = $this->dtoToEntityConverter->convert($entity, $dto);
        $this->repository->save($entity);

        // If this is an administrable content, we want to save a log of any modification
        if ($entity instanceof AdministrableResourceEntityInterface) {
            $log = $this->administrationLogRepository->createFromAdministrableResourceEntity(
                $entity,
                AdministrationLog::ACTION_UPDATE,
                $this->getResourceName(\get_class($entity))
            );
            $this->administrationLogRepository->save($log);
        }

        $this->repository->flush();

        $this->generateMetadata($entity);

        return $this->getJsonResponseForData(
            $entity,
            Response::HTTP_OK,
            null,
            false,
            [],
            false,
        );
    }

    protected function generateMetadata(EntityInterface $entity): void
    {
    }
}
