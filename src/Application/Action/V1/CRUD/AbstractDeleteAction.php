<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD;

use App\Application\Action\V1\AbstractAction;
use App\Application\Action\V1\CRUD\Traits\ExtractEntityFromRequestTrait;
use App\Domain\Entity\AdministrableResourceEntityInterface;
use App\Domain\Entity\Homepage\AdministrationLog;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\CRUD\DeleteRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractDeleteAction extends AbstractAction
{
    use ExtractEntityFromRequestTrait;

    public function __construct(
        private readonly DeleteRepositoryInterface $repository,
        private readonly AdministrationLogRepositoryInterface $administrationLogRepository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
        $this->resourceName = null;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $entity = $this->extractEntityFromRequest($request, $this->repository);
        $this->repository->delete($entity);

        // If this is an administrable content, we want to save a log of any modification
        if ($entity instanceof AdministrableResourceEntityInterface) {
            $log = $this->administrationLogRepository->createFromAdministrableResourceEntity(
                $entity,
                AdministrationLog::ACTION_DELETE,
                $this->getResourceName(\get_class($entity))
            );
            $this->administrationLogRepository->save($log);
        }

        $this->repository->flush();

        return $this->getJsonResponseForData(null, Response::HTTP_NO_CONTENT, null, false, [], false);
    }
}
