<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD;

use App\Application\Action\V1\AbstractAction;
use App\Application\Action\V1\CRUD\Traits\ExtractEntityFromRequestTrait;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractGetAction extends AbstractAction
{
    use ExtractEntityFromRequestTrait;

    public function __construct(
        private readonly GetRepositoryInterface $repository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
        $this->resourceName = null;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $entity = $this->extractEntityFromRequest($request, $this->repository);

        return $this->getJsonResponseForData(
            $entity,
            Response::HTTP_OK,
            null,
            false,
            [],
            !$request->isNoCache()
        );
    }
}
