<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD\Traits;

trait GetResourceNameTrait
{
    protected readonly ?string $resourceName;

    private function getResourceName(string $entityClassName): string
    {
        if (null !== $this->resourceName) {
            return $this->resourceName;
        }

        $bits = explode('\\', $entityClassName);
        $lastBit = end($bits);

        return $this->camelCaseToSpinalCase($lastBit);
    }

    private function camelCaseToSpinalCase(string $camelCase): string
    {
        $cased = preg_replace('/[A-Z]/', '-\\0', lcfirst($camelCase));
        if (null === $cased) {
            throw new \LogicException(sprintf('Could not convert "%s" to spinal-case', $camelCase));
        }

        return mb_strtolower($cased);
    }
}
