<?php

declare(strict_types=1);

namespace App\Application\Action\V1\CRUD\Traits;

use App\Domain\Entity\EntityInterface;
use App\Infrastructure\Repository\CRUD\GetRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Webmozart\Assert\Assert;

trait ExtractEntityFromRequestTrait
{
    use GetResourceNameTrait;

    private function extractEntityFromRequest(
        Request $request,
        GetRepositoryInterface $repository,
        string $context = GetRepositoryInterface::CONTEXT_WRITE
    ): EntityInterface {
        Assert::inArray($context, GetRepositoryInterface::CONTEXTS);

        /**
         * @var string $id
         */
        $id = $request->attributes->get('id');
        $entity = $repository->findOneById($id, $context);
        if (null === $entity) {
            $resourceName = $this->getResourceName($repository->getClassName());

            throw new NotFoundHttpException(sprintf('Resource of type "%s" with id "%s" could not be found.', $resourceName, $id));
        }

        return $entity;
    }
}
