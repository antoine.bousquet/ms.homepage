<?php

declare(strict_types=1);

namespace App\Application\Action\V1\AdministrationLog;

use App\Application\Action\V1\CRUD\AbstractListAction;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;

class ListAction extends AbstractListAction
{
    public function __construct(
        AdministrationLogRepositoryInterface $repository,
        CamelCaseToSnakeCaseNameConverter $nameConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $nameConverter, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
