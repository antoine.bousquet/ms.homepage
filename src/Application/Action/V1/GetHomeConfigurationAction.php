<?php

declare(strict_types=1);

namespace App\Application\Action\V1;

use App\Domain\Block\BlockHandlerInterface;
use App\Domain\Enum\HomeBlocks;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Webmozart\Assert\Assert;

final class GetHomeConfigurationAction extends AbstractAction
{
    /**
     * @param iterable<BlockHandlerInterface> $blocks
     */
    public function __construct(
        private iterable $blocks,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
        private readonly LoggerInterface $logger = new NullLogger(),
    ) {
        parent::__construct($serializer, $cacheResponseHeaderTimeToLive);
    }

    /**
     * @throws BadRequestHttpException If incoming data is invalid
     */
    public function __invoke(Request $request): JsonResponse
    {
        $platform = $this->extractPlatformFromRequest($request);
        $blocksFromRequest = $this->extractBlocksFromRequest($request);

        foreach ($blocksFromRequest as $block) {
            Assert::inArray($block, HomeBlocks::casesString()); // @phpstan-ignore-line
        }

        $content = [];

        foreach ($this->blocks as $block) {
            if (\in_array($block->getName(), $blocksFromRequest, true)) {
                $content[$block->getName()] = $block->getContent($platform);
            }
        }

        return $this->getJsonResponseForData($content, Response::HTTP_OK, 'GET_CONFIGURATION', true);
    }

    /**
     * @return array<string>
     */
    private function extractBlocksFromRequest(Request $request): array
    {
        /**
         * @var array<string> $allBlocks
         */
        $allBlocks = HomeBlocks::casesString();
        $blocks = $request->get('blocks');
        if (\is_string($blocks)) {
            $blocks = explode(',', $blocks);
            if (\count(array_intersect($blocks, $allBlocks)) !== \count($blocks)) {
                $this->logger->warning(sprintf(
                    'The following blocks have been requested but do not exist: %s',
                    implode(', ', array_diff($blocks, $allBlocks))
                ));
                $blocks = array_intersect($blocks, $allBlocks);
            }
        } elseif (null !== $blocks) {
            throw new BadRequestHttpException(sprintf('Invalid block list provided, must be a list of block names separated by comas among the following list: %s', implode(', ', $allBlocks)));
        }

        return $blocks ?? $allBlocks;
    }
}
