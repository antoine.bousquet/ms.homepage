<?php

declare(strict_types=1);

namespace App\Application\Action\V1\Normalizer;

use App\Domain\Service\MetadataService;
use Manomano\ApiBundle\Handler\IgnoredAttributesHandler;
use Manomano\ApiSpecs\Pagination\PaginatedContentInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class ContentWrappingNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    public const ROOT_KEY = '_api_root';
    public const CONTENT_KEY = 'content';
    public const PAGINATION_KEY = 'pagination';
    public const METADATA_KEY = 'metadata';

    private IgnoredAttributesHandler $ignoredAttributesHandler;

    private NormalizerInterface $normalizer;

    public function __construct(
        private readonly bool $skipNullValues,
        private readonly MetadataService $metadataService,
    ) {
    }

    public function setIgnoredAttributesHandler(IgnoredAttributesHandler $ignoredAttributesHandler): void
    {
        $this->ignoredAttributesHandler = $ignoredAttributesHandler;
    }

    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param object $object
     * @param array{
     *     _api_root?: bool,
     *     ignored_attributes?: array<mixed>,
     *     skip_null_values?: bool,
     * } $context
     */
    public function normalize(mixed $object, string $format = null, array $context = []): mixed
    {
        $isRoot = $context[self::ROOT_KEY] ?? true;
        $context[self::ROOT_KEY] = false;

        $context[AbstractObjectNormalizer::SKIP_NULL_VALUES] =
            $context[AbstractObjectNormalizer::SKIP_NULL_VALUES] ??
            $this->skipNullValues;

        if (true === $isRoot) {
            if (null !== $this->metadataService->getMetadata()) {
                $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = $this->getIgnoredAttributes($object, $context);

                return [
                    self::CONTENT_KEY => $this->normalizer->normalize($object, $format, $context),
                    self::METADATA_KEY => $this->normalizer->normalize($this->metadataService->getMetadata(), $format, $context),
                ];
            }
            if ($object instanceof PaginatedContentInterface) {
                $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = $this->getIgnoredAttributes($object->getItems(), $context);

                return [
                    self::CONTENT_KEY => $this->normalizer->normalize($object->getItems(), $format, $context),
                    self::PAGINATION_KEY => $this->normalizer->normalize($object->getPagination(), $format, $context),
                ];
            }

            $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = $this->getIgnoredAttributes($object, $context);

            return [self::CONTENT_KEY => $this->normalizer->normalize($object, $format, $context)];
        }

        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = $this->getIgnoredAttributes($object, $context);

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param array{_api_root?: bool} $context
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $context[self::ROOT_KEY] ?? true;
    }

    /**
     * @param object|array<object>                     $object
     * @param array{ignored_attributes?: array<mixed>} $context
     *
     * @return array<mixed>
     */
    private function getIgnoredAttributes(object|array|null $object, array $context): array
    {
        $contextIgnoredAttributes = $context[AbstractNormalizer::IGNORED_ATTRIBUTES] ?? [];
        if (null === $object) {
            return $contextIgnoredAttributes;
        }

        return [
            ...$contextIgnoredAttributes,
            ...$this->ignoredAttributesHandler->getFromFieldsToDisplay($object),
        ];
    }
}
