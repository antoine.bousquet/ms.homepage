<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedBrand;

use App\Application\Action\V1\CRUD\AbstractCreateAction;
use App\Domain\Converter\DtoToEntityConverter;
use App\Domain\DTO\PromotedBrandInputDTO;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Symfony\Component\Serializer\SerializerInterface;

final class CreateAction extends AbstractCreateAction
{
    public function __construct(
        PromotedBrandRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        RequestToDTOConverter $requestToDTOConverter,
        DtoToEntityConverter $dtoToEntityConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct(
            PromotedBrandInputDTO::class,
            $repository,
            $administrationLogRepository,
            $requestToDTOConverter,
            $dtoToEntityConverter,
            $serializer,
            $cacheResponseHeaderTimeToLive,
        );
    }
}
