<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedBrand;

use App\Application\Action\V1\CRUD\AbstractGetAction;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class GetAction extends AbstractGetAction
{
    public function __construct(
        PromotedBrandRepositoryInterface $repository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
