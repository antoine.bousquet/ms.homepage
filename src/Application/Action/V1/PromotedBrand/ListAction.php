<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedBrand;

use App\Application\Action\V1\CRUD\AbstractListAction;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;

final class ListAction extends AbstractListAction
{
    public function __construct(
        PromotedBrandRepositoryInterface $repository,
        CamelCaseToSnakeCaseNameConverter $nameConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $nameConverter, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
