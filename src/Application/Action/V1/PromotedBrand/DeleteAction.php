<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedBrand;

use App\Application\Action\V1\CRUD\AbstractDeleteAction;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\PromotedBrandRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class DeleteAction extends AbstractDeleteAction
{
    public function __construct(
        PromotedBrandRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $administrationLogRepository, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
