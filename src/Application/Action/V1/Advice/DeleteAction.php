<?php

declare(strict_types=1);

namespace App\Application\Action\V1\Advice;

use App\Application\Action\V1\CRUD\AbstractDeleteAction;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\AdviceRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class DeleteAction extends AbstractDeleteAction
{
    public function __construct(
        AdviceRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct(
            $repository,
            $administrationLogRepository,
            $serializer,
            $cacheResponseHeaderTimeToLive,
        );
    }
}
