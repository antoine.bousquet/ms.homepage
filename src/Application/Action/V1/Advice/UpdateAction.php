<?php

declare(strict_types=1);

namespace App\Application\Action\V1\Advice;

use App\Application\Action\V1\CRUD\AbstractUpdateAction;
use App\Domain\Converter\DtoToEntityConverter;
use App\Domain\DTO\AdviceInputDTO;
use App\Infrastructure\Client\Exception\CmsUnavailableException;
use App\Infrastructure\Client\Exception\InvalidClientResponseFormatException;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\AdviceRepositoryInterface;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Serializer\SerializerInterface;

final class UpdateAction extends AbstractUpdateAction
{
    public function __construct(
        AdviceRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        RequestToDTOConverter $requestToDTOConverter,
        DtoToEntityConverter $dtoToEntityConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct(
            AdviceInputDTO::class,
            $repository,
            $administrationLogRepository,
            $requestToDTOConverter,
            $dtoToEntityConverter,
            $serializer,
            $cacheResponseHeaderTimeToLive,
        );
    }

    public function __invoke(Request $request): JsonResponse
    {
        try {
            return parent::__invoke($request);
        } catch (CmsUnavailableException|InvalidClientResponseFormatException $exception) {
            throw new ServiceUnavailableHttpException(null, $exception->getMessage(), $exception);
        }
    }
}
