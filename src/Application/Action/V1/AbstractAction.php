<?php

declare(strict_types=1);

namespace App\Application\Action\V1;

use App\Domain\Enum\Platform;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractAction
{
    public function __construct(
        protected readonly SerializerInterface $serializer,
        protected readonly int $cacheResponseHeaderTimeToLive,
    ) {
    }

    abstract public function __invoke(Request $request): JsonResponse;

    protected function extractPlatformFromRequest(Request $request): string
    {
        $platform = $request->get('platform');
        if (!\is_string($platform) || !\in_array(mb_strtoupper($platform), Platform::casesString(), true)) {
            throw new BadRequestHttpException(sprintf('Query parameter "%s" must be provided, and must be one of the following values: %s', 'platform', implode(', ', Platform::casesString())));
        }

        return $platform;
    }

    /**
     * @param array<string, mixed>                            $headers
     * @param array{Deprecation: string, Sunset: string}|null $deprecation
     */
    protected function getJsonResponseForData(
        mixed $data,
        int $statusCode = Response::HTTP_OK,
        ?string $serializationGroup = null,
        bool $public = false,
        array $headers = [],
        bool $useCache = true,
        ?array $deprecation = null,
    ): JsonResponse {
        $context = (null === $serializationGroup ? [] : [AbstractNormalizer::GROUPS => $serializationGroup]);

        if (null !== $deprecation) {
            $headers = [...$headers, ...$deprecation];
        }

        $response = new JsonResponse(
            $this->serializer->serialize(
                $data,
                'json',
                $context
            ),
            $statusCode,
            $headers,
            true
        );

        if ($public) {
            $response->setPublic();
        }
        if ($useCache) {
            $response->setMaxAge($this->cacheResponseHeaderTimeToLive);
            $response->setSharedMaxAge($this->cacheResponseHeaderTimeToLive);
        }

        return $response;
    }
}
