<?php

declare(strict_types=1);

namespace App\Application\Action\V1\BrandData;

use App\Application\Action\V1\CRUD\AbstractListAction;
use App\Infrastructure\Repository\BrandDataRepositoryInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;

class ListAction extends AbstractListAction
{
    public function __construct(
        BrandDataRepositoryInterface $repository,
        CamelCaseToSnakeCaseNameConverter $nameConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $nameConverter, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
