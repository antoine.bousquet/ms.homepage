<?php

declare(strict_types=1);

namespace App\Application\Action\V1\BrandData;

use App\Application\Action\V1\CRUD\AbstractGetAction;
use App\Infrastructure\Repository\BrandDataRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

class GetAction extends AbstractGetAction
{
    public function __construct(
        BrandDataRepositoryInterface $repository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
