<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedProduct;

use App\Application\Action\V1\CRUD\AbstractGetAction;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class GetAction extends AbstractGetAction
{
    public function __construct(
        PromotedProductRepositoryInterface $repository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
