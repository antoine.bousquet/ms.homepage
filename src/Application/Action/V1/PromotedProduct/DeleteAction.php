<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedProduct;

use App\Application\Action\V1\CRUD\AbstractDeleteAction;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class DeleteAction extends AbstractDeleteAction
{
    public function __construct(
        PromotedProductRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $administrationLogRepository, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
