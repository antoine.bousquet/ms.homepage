<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedProduct;

use App\Application\Action\V1\CRUD\AbstractCreateAction;
use App\Domain\Converter\DtoToEntityConverter;
use App\Domain\DTO\PromotedProductInputDTO;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\Homepage\PromotedProduct;
use App\Domain\Service\MetadataService;
use App\Infrastructure\Client\Exception\InvalidClientResponseFormatException;
use App\Infrastructure\Client\Exception\RhinoReaderUnavailableException;
use App\Infrastructure\Repository\AdministrationLogRepositoryInterface;
use App\Infrastructure\Repository\ProductRepositoryInterface;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;
use Manomano\ApiBundle\Request\RequestToDTOConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Serializer\SerializerInterface;

final class CreateAction extends AbstractCreateAction
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        PromotedProductRepositoryInterface $repository,
        AdministrationLogRepositoryInterface $administrationLogRepository,
        private MetadataService $metadataService,
        RequestToDTOConverter $requestToDTOConverter,
        DtoToEntityConverter $dtoToDoctrineEntityConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct(
            PromotedProductInputDTO::class,
            $repository,
            $administrationLogRepository,
            $requestToDTOConverter,
            $dtoToDoctrineEntityConverter,
            $serializer,
            $cacheResponseHeaderTimeToLive,
        );
    }

    public function __invoke(Request $request): JsonResponse
    {
        try {
            return parent::__invoke($request);
        } catch (RhinoReaderUnavailableException|InvalidClientResponseFormatException $exception) {
            throw new ServiceUnavailableHttpException(null, $exception->getMessage(), $exception);
        }
    }

    /**
     * @param PromotedProduct $promotedProduct
     */
    protected function generateMetadata(EntityInterface $promotedProduct): void
    {
        if (
            false === $this->productRepository->isModelIdMarkedDown(
                $promotedProduct->getModelId(),
                $promotedProduct->getPlatformId()
            )
        ) {
            $this->metadataService->addWarning(sprintf(
                'Price for model with ID #%d is not currently marked down in platform %s',
                $promotedProduct->getModelId(),
                $promotedProduct->getPlatformId()
            ));
        }
    }
}
