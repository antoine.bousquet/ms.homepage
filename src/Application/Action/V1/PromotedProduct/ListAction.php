<?php

declare(strict_types=1);

namespace App\Application\Action\V1\PromotedProduct;

use App\Application\Action\V1\CRUD\AbstractListAction;
use App\Infrastructure\Repository\PromotedProductRepositoryInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;

final class ListAction extends AbstractListAction
{
    public function __construct(
        PromotedProductRepositoryInterface $repository,
        CamelCaseToSnakeCaseNameConverter $nameConverter,
        SerializerInterface $serializer,
        int $cacheResponseHeaderTimeToLive,
    ) {
        parent::__construct($repository, $nameConverter, $serializer, $cacheResponseHeaderTimeToLive);
    }
}
