directive @extends on OBJECT | INTERFACE

directive @key(fields: String!) on OBJECT | INTERFACE

directive @external on FIELD_DEFINITION

directive @requires(fields: String!) on FIELD_DEFINITION

directive @provides(fields: String!) on FIELD_DEFINITION

"""A scalar type representing big integers."""
scalar BigInteger

"""Brand data"""
type Brand @extends @key(fields: "brandId market platform") {
  """Brand identifier"""
  brandId: Int! @external

  """Associated platform"""
  platform: Platform! @external

  """Associated market"""
  market: Market! @external
}

"""One of the available channels"""
enum Channel {
  """Web context"""
  WEB

  """Mobile context"""
  MOBILE
}

"""Content required to display the home page"""
type Homepage @key(fields: "platform market channel") {
  """Associated platform"""
  platform: Platform!

  """Associated market"""
  market: Market!

  """Associated channel"""
  channel: Channel!

  """
  All brands attached to the provided platform/channel/market, sorted by their
  configured position. Capped at 1000 elements, an error is emitted if the
  maximum is reached.
  """
  brands: [Brand!]!

  """
  All advice attached to the provided platform/channel/market, sorted by their
  configured position. Capped at 1000 elements, an error is emitted if the
  maximum is reached.
  """
  advice: [Tipsheet!]!

  """
  All suggested categories attached to the provided platform/channel/market, sorted by their configured position.
  """
  suggestedCategories: [SuggestedCategory!]!

  """
  All promoted products attached to the provided platform/channel/market,
  fetched from their model identifiers, sorted by their configured position.
  Capped at 1000 elements, an error is emitted if the maximum is reached.
  """
  promotedProducts: ProductCollection!
}

"""One of the available markets"""
enum Market {
  """Business to Customer"""
  B2C

  """Business to Business"""
  B2B
}

"""One of the available platforms"""
enum Platform {
  """France"""
  FR

  """Spain"""
  ES

  """Italy"""
  IT

  """Great Britain"""
  GB

  """Germany"""
  DE
}

"""Product collection external entity"""
type ProductCollection @extends @key(fields: "platform isPro modelIds limit") {
  """Associated platform"""
  platform: Platform! @external

  """Whether these products are reserved to professional accounts"""
  isPro: Boolean! @external

  """Identifiers of the searched models"""
  modelIds: [BigInteger!]! @external

  """Maximum number of products to fetch"""
  limit: Int! @external
}

"""The global query object"""
type Query {
  """Homepage data"""
  homepage(
    """Associated platform"""
    platform: Platform!

    """Associated market"""
    market: Market!

    """Associated channel"""
    channel: Channel!
  ): Homepage

  """
  Legacy - will fetch homepage brands for the provided platform, on B2C market
  and WEB channel, sorted by their configured position. Capped at 1000 elements,
  an error is emitted if the maximum is reached.
  """
  homepageB2cBrands(
    """Associated platform"""
    platform: Platform!
  ): [Brand!]! @deprecated(reason: "Add nullability. Use query \"homepage\" instead. Removal on 2023-01-10")

  """
  Legacy - will fetch homepage brands for the provided platform, on B2C market
  and WEB channel, sorted by their configured position. Capped at 1000 elements,
  an error is emitted if the maximum is reached.
  """
  homepageB2cBrandsV1(
    """Associated platform"""
    platform: Platform!
  ): [Brand!] @deprecated(reason: "Full rework. Use query \"homepage\" instead. Removal on 2023-10-01")

  """
  Legacy - will fetch homepage tipsheets for the provided platform, on B2C
  market and WEB channel, sorted by their configured position. Capped at 1000
  elements, an error is emitted if the maximum is reached.
  """
  homepageB2cTipsheets(
    """Associated platform"""
    platform: Platform!
  ): [Tipsheet!]! @deprecated(reason: "Add nullability. Use query \"homepage\" instead. Removal on 2023-01-10")

  """
  Legacy - will fetch homepage tipsheets for the provided platform, on B2C
  market and WEB channel, sorted by their configured position. Capped at 1000
  elements, an error is emitted if the maximum is reached.
  """
  homepageB2cTipsheetsV1(
    """Associated platform"""
    platform: Platform!
  ): [Tipsheet!] @deprecated(reason: "Full rework. Use query \"homepage\" instead. Removal on 2023-10-01")

  """The "_service" query property, as required by Apollo Federation"""
  _service: _Service!

  """The "_entities" query property, as required by Apollo Federation"""
  _entities(
    """
    The "representations" argument for "_entities", as required by Apollo Federation
    """
    representations: [_Any!]!
  ): [_Entity]
}

"""
Category data displayed in the homepage.
This entity intentionally does not reference the main "Category" entity, because
considering its planned usage (exclusive to the homepage), there is no obvious
interest in doing so.
The choice has been made to also not extend the "Category" entity, as the
homepage data presented here should be exclusive to the homepage context.
Because it should remain exclusive to the homepage context, this is also not a shareable or extensible entity.
"""
type SuggestedCategory {
  """Identifier of the associated category"""
  id: ID!

  """Identifier of the top-most category (family) to display"""
  familyId: ID!

  """
  Name to use, not necessary the same as the associated "Category"
  """
  name: String!

  """All image-related data"""
  image: SuggestedCategoryImage!

  """
  Relative URL to the category page, not necessary the same as the slug of the associated "Category"
  """
  url: String!
}

"""
Image data for the suggested categories.
This is not a shareable or extensible entity as its planned usage limits it to the homepage.
"""
type SuggestedCategoryImage {
  """URL path to the regular image"""
  smallUrl: String!
}

"""Tipsheet data"""
type Tipsheet @extends @key(fields: "id") {
  """Tipsheet identifier"""
  id: Int! @external
}

"""The "_Any" scalar type, as required by Apollo Federation"""
scalar _Any

"""The "_Entity" scalar type, as required by Apollo Federation"""
union _Entity = Homepage | Brand | Tipsheet | ProductCollection

"""The "_Service" object type, as required by Apollo Federation"""
type _Service {
  """
  The "sdl" property of the "_Service" object type, as required by Apollo Federation
  """
  sdl: String
}
