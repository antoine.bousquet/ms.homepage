# Welcome to `ms.homepage`

This is a simplified and not-installable version of the homepage microservice used by *[ManoMano](https://www.manomano.com/)*.
This repository is a support repository for the [following presentation](https://docs.google.com/presentation/d/1F4L6IhhAOVVeaSsh5PVjYm85OB072KcSoA8BcCbKYIQ/edit?usp=sharing).

The idea is to highlight the usage of *[GraphQL](https://graphql.org/)* and *[Apollo Federation](https://www.apollographql.com/docs/federation/)*
in a *[PHP](https://www.php.net/)* project, in this case using the *[Symfony framework](https://symfony.com/)*.

# Installation

This project uses *ManoMano* dependencies that are only accessible for members of the company. As such, installing it proper
is impossible for a non-ManoMano user.

The project was initiated from a real-life microservice though, and can be a solid basis for your own *GraphQL* implementation.
