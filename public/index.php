<?php

declare(strict_types=1);

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

if (class_exists(Dotenv::class)) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

$_SERVER['APP_ENV'] = $_SERVER['APP_ENV'] ?? 'prod';
$_SERVER['APP_DEBUG'] = $_SERVER['APP_DEBUG'] ?? ('prod' !== $_SERVER['APP_ENV']);

if ('blackfire' === ($_SERVER['DEBUG_EXTENSION'] ?? '') && isset($_SERVER['HTTP_BLACKFIRETRIGGER'])) {
    $blackfire = new \Blackfire\Client();
    $probe = $blackfire->createProbe();
    register_shutdown_function(function () use ($blackfire, $probe): void {
        $profile = $blackfire->endProbe($probe);
    });
}

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
