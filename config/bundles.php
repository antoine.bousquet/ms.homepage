<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Manomano\DocBundle\ManomanoDocBundle::class => ['all' => true],
    Manomano\ApiBundle\ManomanoApiBundle::class => ['all' => true],
    Manomano\ObservabilityBundle\ManomanoObservabilityBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['dev' => true, 'test' => true],
    Manomano\RolloutBundle\ManomanoRolloutBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
    Manomano\SecurityBundle\ManomanoSecurityBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Http\HttplugBundle\HttplugBundle::class => ['all' => true],
    Manomano\ClientBundle\ManomanoClientBundle::class => ['all' => true],
    Manomano\AvailabilityBundle\ManomanoAvailabilityBundle::class => ['all' => true],
];
